# Task List #

[X] Carregar Objeto

## Transformações ##

[X] Translação

[X] Rotação

[X] Escala

[X] View Port

## Projeções ##

[X] Ortográfica 3 vistas

[X] Gabinete

[X] Cavaleira

[X] Perspectiva 1 ponto de fuga

[X] Faces Ocultas

## Iluminação##

[X] Flat 
 
[ ] Phong c/Z-Buffer

[ ] Gouraud

#Notas#

## Flat ##
Cor Igual

### Gourard ###

Scan line: yMax,xMin,incX, rMin,gMin,bMin,rInc,gInc,bInc

### ---------------------------------------------------###
Projeção Ortográfica retira o z (5,1,6) -> (5,1)

arquivo .obj objeto 3d

v -> vetor (x,y,z)

v 0.5 0.72 0.51

nv -> normal do vértice

vt -> ponto de textura 

f -> faces primeiro valor representa o índice do ponto

f 1//1 2//2 3//6

primeiro valor indica qual e o vértice

o segundo indica o index do vetor normal 

coordenadas normalizadas(entre 0 e 1) fazer viewport para a tela

valores double

possuir estrutura de vértices originais e atuais contendo (x,y,z)

lista de faces com índice de vértices 

cada face possui um vetor normal

para a sequencia de pontos A B C 

montar vetor AB e AC

AB = B - A = (x1,y1,z1)

AC = C - A = (x2,y2,z2)

|  i   j    k  |

|x1 y1 z1 |

|x2 y2 z2|

na leitura somar x = W/2 e y = H/2 para mover para o centro

##Carregar##

lista de vértices

lista de faces -> index dos vértices

estrutura pode ter faces com 3 ou mais pontos