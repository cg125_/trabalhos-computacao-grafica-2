﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3D
{
    class Aresta
    {
        private Vector3 p1, p2;
        private int yMin;
        private double yMax;
        private double xMin, incX; // x do y min
        private double zMin, zMax,incZ;
        private double ri,gi,bi;
        private double rf, gf, bf;
        private double incr, incg, incb;
        
        public Aresta(Vector3 p1, Vector3 p2)
        {
            double Xmax;
            yMin = (int)Math.Min(p1.getY(), p2.getY());
            
            yMin = yMin > 0 ? yMin : 0;
            this.p1 = p1;
            this.p2 = p2;
            if (p1.getY() < p2.getY())
            {
                xMin = p1.getX();
                Xmax = p2.getX();
                zMin = p1.getZ();
                zMax = p2.getZ();
            }
            else
            {
                xMin = p2.getX();
                Xmax = p1.getX();
                zMin = p2.getZ();
                zMax = p1.getZ();
            }
            yMax = Math.Max(p1.getY(), p2.getY());
            
            incX = (yMax - yMin) > 0 ? (Xmax - xMin) / (yMax - yMin) : 0;
            incZ = (yMax - yMin) > 0 ? (zMax - zMin) / (yMax - yMin) : 0;
        }
        public Aresta(Vector3 p1, Vector3 p2, Color ci,Color cf)
        {
            ri = ci.R;
            gi = ci.G;
            bi = ci.B;

            rf = cf.R;
            gf = cf.G;
            bf = cf.B;

            double Xmax;
            yMin = (int)Math.Min(p1.getY(), p2.getY());

            yMin = yMin > 0 ? yMin : 0;
            this.p1 = p1;
            this.p2 = p2;
            if (p1.getY() < p2.getY())
            {
                xMin = p1.getX();
                Xmax = p2.getX();
                zMin = p1.getZ();
                zMax = p2.getZ();
            }
            else
            {
                xMin = p2.getX();
                Xmax = p1.getX();
                zMin = p2.getZ();
                zMax = p1.getZ();
            }
            yMax = Math.Max(p1.getY(), p2.getY());
            incX = (yMax - yMin) > 0 ? (Xmax - xMin) / (yMax - yMin) : 0;
            incZ = (yMax - yMin) > 0 ? (zMax - zMin) / (yMax - yMin) : 0;
            
            incr = (yMax - yMin) > 0 ? (rf - ri) / (yMax - yMin) : 0;
            incg = (yMax - yMin) > 0 ? (gf - gi) / (yMax - yMin) : 0;
            incb = (yMax - yMin) > 0 ? (bf - bi) / (yMax - yMin) : 0;
            
            /*
            incr = (Xmax - xMin) > 0 ? (rf - ri) / (Xmax - xMin) : 0;
            incg = (Xmax - xMin) > 0 ? (gf - gi) / (Xmax - xMin) : 0;
            incb = (Xmax - xMin) > 0 ? (bf - bi) / (Xmax - xMin) : 0;
            */
        }

        public Vector3 getP1() { return p1; }
        public Vector3 getP2() { return p2; }
        public int getYmin(){ return yMin; }        
        public double getYmax() { return yMax; }
        public double getXmin() { return xMin; }
        public double getIncX() { return incX; }
        public Color getCor() { return Color.FromArgb((int)ri, (int)gi, (int)bi); }
        public void setXmin(double x) { xMin = x; }        
        public void setCor(Color c)
        {
            ri = c.R;
            gi = c.G;
            bi = c.B;
        }
        public void incR()
        {
            ri = incr + ri <= 255 ? incr + ri : 255;
            ri = ri >= 0 ? ri : 0;
        }
        public void incG()
        {
            gi = incg + gi <= 255 ? incg + gi : 255;
            gi = gi >= 0 ? gi : 0;
        }
        public void incB()
        {
            bi = incb + bi <= 255 ? incb + bi : 255;
            bi = bi >= 0 ? bi : 0;
        }
        public void incCor()
        {
            incR();
            incG();
            incB();
        }
        public void incCor(double ir,double ig, double ib)
        {
            ri = ri + ir > 255 ? 255 : ri + ir;
            gi = gi + ig > 255 ? 255 : gi + ig;
            bi = bi + ib > 255 ? 255 : bi + ib;
        }
    }
}
