﻿using System;

namespace _3D
{
    class Vector3
    {
        private double x;
        private double y;
        private double z;

        public Vector3()
        {
            x = y = z = 0;
        }

        public Vector3(double x, double y, double z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }
    
        public double getX()
        {
            return x;
        }

        public double getY()
        {
            return y;
        }

        public double getZ()
        {
            return z;
        }

        public void setX(double x)
        {
            this.x = x;
        }

        public void setY(double y)
        {
            this.y = y;
        }

        public void setZ(double z)
        {
            this.z = z;
        }

        public void normalize()
        {
            double normalizar = Math.Sqrt(Math.Pow(x, 2) + Math.Pow(y, 2) + Math.Pow(z, 2));
            if (normalizar != 0)
            {
                x /= normalizar;
                y /= normalizar;
                z /= normalizar;
            }
        }
        public double norma()
        {
            return Math.Sqrt(Math.Pow(x, 2) + Math.Pow(y, 2) + Math.Pow(z, 2));
        }
    }
}