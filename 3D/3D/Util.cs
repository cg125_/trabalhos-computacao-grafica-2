﻿
using _3D.UI;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

namespace _3D
{
    class Util
    {
        public unsafe static void preencher(Bitmap img, Color c)
        {
            int W = img.Width;
            int H = img.Height;
            BitmapData bmpData = img.LockBits(new Rectangle(0, 0, W, H), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);

            byte* ptr = (byte*)bmpData.Scan0.ToPointer();
            int padding = bmpData.Stride - (W * 3);

            for (int y = 0; y < H; y++)
            {
                for (int x = 0; x < W; x++)
                {
                    *(ptr++) = (byte)c.B;
                    *(ptr++) = (byte)c.G;
                    *(ptr++) = (byte)c.R;
                }
                ptr += padding;
            }

            img.UnlockBits(bmpData);
        }
        public unsafe static byte* getPos(byte* pIni, int TX, int TY, int W, int padding)
        {
            byte* pos = pIni;
            pos += (W * 3 + padding) * TY + (TX * 3);
            return pos;
        }
        public unsafe static void setPixel(byte* pIni, int TX, int TY, int W, int padding, Color c, int limW,int limH)
        {
            if (TX < 0 || TY < 0 || TX >= limW || TY >= limH)
                return;

            byte* ptr = pIni;
            ptr += (W * 3 + padding) * TY + (TX * 3);
            *(ptr++) = c.B;
            *(ptr++) = c.G;
            *(ptr++) = c.R;
        }
        public unsafe static Color getPixel(byte* pIni, int TX, int TY, int W, int padding)
        {
            if (TX < 0 || TY < 0 || TX >= FRMPrincipal.getW() || TY >= FRMPrincipal.getH())
                return Color.White;

            byte* ptr = pIni;
            ptr += (W * 3 + padding) * TY + (TX * 3);
            int b = *(ptr++);
            int g = *(ptr++);
            int r = *(ptr++);
            return Color.FromArgb(r, g, b);
        }
        public static double[,] multiplicar(double[,] mat, double[,] mat2)
        {
            double[,] matR = new double[4, 4];
            for (int i = 0; i < 4; i++)
                for (int j = 0; j < 4; j++)
                    for (int k = 0; k < 4; k++)
                        matR[i, j] += mat[i, k] * mat2[k, j];
            return matR;
        }
        public static Objeto carrega(StreamReader rs,out int vetores,out int faces,out int vetor_normal)
        {
            vetores = faces = vetor_normal = 0;
            double CX = FRMPrincipal.getW() / 5;
            double CY = FRMPrincipal.getH() / 5;

            string linha;
            
            List<Vector3> pts = new List<Vector3>();
            List<Face> fcs = new List<Face>();
            List<Vector3> vns = new List<Vector3>();
            
            string[] filtro = { " ", "//","/" };

            while ((linha = rs.ReadLine()) != null)
            {
                string[] pString = linha.Split(filtro, StringSplitOptions.RemoveEmptyEntries);
                if (pString.Length == 0)
                    continue;

                for (int i = 0; i < pString.Length; i++)
                {
                    pString[i] = pString[i].Replace('.', ',');
                }
                if (pString[0].Equals("v"))
                {
                    vetores++;
                    Vector3 v = new Vector3(Double.Parse(pString[1]) + CX, Double.Parse(pString[2]) + CY, Double.Parse(pString[3]) + (CX+CY)/2);
                    pts.Add(v);
                }
                if (pString[0].Equals("vn"))
                {
                    Vector3 vn = new Vector3(Double.Parse(pString[1]) + CX, Double.Parse(pString[2]) + CY, Double.Parse(pString[3]) + (CX + CY) / 2);
                    vns.Add(vn);
                    vetor_normal++;
                }
                if (pString[0].Equals("f"))
                {
                    faces++;
                    if (pString.Length % 2 == 1) //f 1/1 2/2 3/3 
                    {
                        List<int> pface = new List<int>();
                        for (int j = 1; j < pString.Length; j += 2)
                        {
                            int index = int.Parse(pString[j]);
                            pface.Add(index - 1);
                        }
                        fcs.Add(new Face(pface));
                    }
                    else
                    {
                        List<int> pface = new List<int>();
                        for (int j = 1; j < pString.Length; j += 3)//f 1/1/1 2/2/2 3/3/3
                        {
                            int index = int.Parse(pString[j]);
                            pface.Add(index - 1);
                        }
                        fcs.Add(new Face(pface));
                    }
                }
            }
            return new Objeto(pts,fcs,FRMPrincipal.getEscalaW(), FRMPrincipal.getEscalaH());
        }   
        public unsafe static void reta2D(int x1, int y1, int x2, int y2, Bitmap img, Color c,BitmapData bmpData,int limW,int limH)
        {

            int H = img.Height;
            int W = img.Width;
            int dx = x2 - x1;
            int dy = y2 - y1;

            int declive = 1;
            if (Math.Abs(dy) < Math.Abs(dx))
            {
                if (x2 < x1)
                {
                    reta2D(x2, y2, x1, y1, img, c,bmpData,limW,limH);
                    return;
                }
               
                int padding = bmpData.Stride - (W * 3);
                byte* ptrIni = (byte*)bmpData.Scan0.ToPointer();

                if (y2 < y1)
                {
                    dy *= -1;
                    declive = -1;
                }

                int incE = 2 * dy;
                int incNE = 2 * dy - 2 * dx;
                int d = 2 * dy - dx;

                int x, y;

                y = y1;
                for (x = x1; x < x2; x++)
                {

                    Util.setPixel(ptrIni, x, y, W, padding, c, limW,limH);
                    if (d <= 0)
                        d += incE;
                    else
                    {
                        d += incNE;
                        y += declive;
                    }
                }
            }
            else
            {
                if (y2 < y1)
                {
                    reta2D(x2, y2, x1, y1, img, c,bmpData,limW,limH);
                    return;
                }
                int padding = bmpData.Stride - (W * 3);
                byte* ptrIni = (byte*)bmpData.Scan0.ToPointer();

                if (x2 < x1)
                {
                    dx *= -1;
                    declive = -1;
                }

                int incE = 2 * dx;
                int incNE = 2 * dx - 2 * dy;
                int d = 2 * dx - dy;

                int x, y;

                x = x1;
                for (y = y1; y < y2; y++)
                {
                    Util.setPixel(ptrIni, x, y, W, padding, c,limW,limH);
                    if (d <= 0)
                        d += incE;
                    else
                    {
                        d += incNE;
                        x += declive;
                    }
                }
            }
        }
        public static double DeltaXY(Point p1,Point p2)
        {
            return p2.X - p1.X + p2.Y - p1.Y;
        }
    }
}