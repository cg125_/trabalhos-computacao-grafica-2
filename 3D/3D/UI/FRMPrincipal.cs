﻿using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace _3D.UI
{
    public partial class FRMPrincipal : Form
    {        
        #region Variaveis       
        #region Controle Mouse/Teclado
        private Point pt1, pt2;
        private bool isClicked;
        public bool isCtrl;
        #endregion
        #region Bitmaps
        private static int W;
        private static int H;
        private static double escalaImgW;
        private static double escalaImgH;
        private Bitmap imgPrincipal;
        private Bitmap imgXY;
        private Bitmap imgXZ;
        private Bitmap imgYZ;
        #endregion
        private Color corWire;

        private Objeto objeto;
        private long timePbPrincipalInicio;
        private long timePbPrincipalFim;
        private long timePbPrincipalFator;
        private int ops;
        Vector3 luz;
        #endregion
        #region Construtor, Gets e Formload
        public FRMPrincipal()
        {
            InitializeComponent();
        }
        private void FRMPrincipal_Load(object sender, EventArgs e)
        {
            //tempo
            timePbPrincipalFator = 5;
            //Imagem
            W = pbPrincipal.Width;
            H = pbPrincipal.Height;
            escalaImgW = (double)pbXY.Width / W;
            escalaImgH = (double)pbXY.Height / H;
            imgPrincipal = new Bitmap(getW(), getH());
            imgXY = new Bitmap((int)(getW() * escalaImgW), (int)(getH() * escalaImgH));
            imgXZ = new Bitmap((int)(getW() * escalaImgW), (int)(getH() * escalaImgH));
            imgYZ = new Bitmap((int)(getW() * escalaImgW), (int)(getH() * escalaImgH));
            corWire = Color.Blue;
            //valores variaveis
            nudAngulo.Value = 5;
            //Tool Strip
            toolStripButtonSeta_Click(null, null);
            //Luz 
            luz = new Vector3(0,0,1);
            /*
            nudX.Value = (int)luz.getX();
            nudY.Value = (int)luz.getY();
            nudZ.Value = (int)luz.getZ();
            */
            nudX.Value = (int)0;
            nudY.Value = (int)0;
            nudZ.Value = (int)1; 
        }
        public static int getW() { return W; }
        public static int getH() { return H; }
        public static double getEscalaW() { return escalaImgW; }
        public static double getEscalaH() { return escalaImgH; }
        #endregion
        #region MenuStrip
        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int vetores = 0;
            int faces = 0;
            int vns = 0;
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                StreamReader sr = new StreamReader(openFileDialog.FileName);
                objeto = Util.carrega(sr, out vetores, out faces,out vns);
                sr.Close();
                if (objeto != null)
                {
                    /*
                    Vector3 c = objeto.getCentro();
                    objeto.translacao(-c.getX(), -c.getY(), -c.getZ());
                    objeto.rotacao(0, 0, 180);
                    objeto.translacao(c.getX(), c.getY(), c.getZ());
                    objeto.updateVNFaces();
                    */

                    updateTelas(corWire);
                }
            }
            tslObjInfo.Text = "Vetores:" + vetores + " Faces:" + faces + " Vetores Normais:" + vns;

        }
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }
        private void manualToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FRMManual frm = new FRMManual();
            frm.Show();
        }
        #endregion
        #region ToolStrip
        private void clearSelection()
        {
            toolStripButtonSeta.BackColor = Color.FromArgb(0, 0xff, 0xff, 0xff);
            toolStripButtonTranslacao.BackColor = Color.FromArgb(0, 0xff, 0xff, 0xff);
            toolStripButtonRotacao.BackColor = Color.FromArgb(0, 0xff, 0xff, 0xff);
            toolStripButtonEscala.BackColor = Color.FromArgb(0, 0xff, 0xff, 0xff);
        }
        private void toolStripButtonSeta_Click(object sender, EventArgs e)
        {
            clearSelection();
            pbPrincipal.Cursor = Cursors.Arrow;
            ops = 0;
            toolStripButtonSeta.BackColor = Color.FromArgb(255, 0xaa, 0xaa, 0xaa);
        }
        private void toolStripButtonTranslacao_Click(object sender, EventArgs e)
        {
            clearSelection();
            pbPrincipal.Cursor = Cursors.Hand;
            ops = 1;
            toolStripButtonTranslacao.BackColor = Color.FromArgb(255, 0xaa, 0xaa, 0xaa);
        }
        private void toolStripButtonRotacao_Click(object sender, EventArgs e)
        {
            clearSelection();
            pbPrincipal.Cursor = Cursors.Hand;
            ops = 2;
            toolStripButtonRotacao.BackColor = Color.FromArgb(255, 0xaa, 0xaa, 0xaa);
        }
        private void toolStripButtonEscala_Click(object sender, EventArgs e)
        {
            clearSelection();
            pbPrincipal.Cursor = Cursors.Hand;
            ops = 3;
            toolStripButtonEscala.BackColor = Color.FromArgb(255, 0xaa, 0xaa, 0xaa);
        }
        private void toolStripButtonZoomIn_Click(object sender, EventArgs e)
        {
            if (objeto == null)
                return;

            Vector3 c = objeto.getCentro();
            objeto.translacao(-c.getX(), -c.getY(), -c.getZ());
            objeto.escala(1.25, 1.25, 1.25);
            objeto.translacao(c.getX(), c.getY(), c.getZ());
            updateTelas(corWire);
        }
        private void toolStripButtonZoomOut_Click(object sender, EventArgs e)
        {
            if (objeto == null)
                return;

            Vector3 c = objeto.getCentro();
            objeto.translacao(-c.getX(), -c.getY(), -c.getZ());
            objeto.escala(0.75, 0.75, 0.75);
            objeto.translacao(c.getX(), c.getY(), c.getZ());
            updateTelas(corWire);
        }

        private void toolStripButtonCor_Click(object sender, EventArgs e)
        {
            if (colorDialog.ShowDialog() == DialogResult.OK)
            {
                corWire = colorDialog.Color;
                updateTelas(corWire);
            }
        }
        #endregion
        #region Auxiliar Transformações
        private void XYZTranslacao(out double x, out double y, out double z)
        {
            x = 0;
            y = 0;
            z = 0;

            if (rbX.Checked)
            {
                x = pt2.X - pt1.X;
            }
            else if (rbY.Checked)
            {
                y = pt2.Y - pt1.Y;
            }
            else if (rbZ.Checked)
            {
                if (isCtrl)
                    z = pt2.Y - pt1.Y;
            }
            else
            {
                x = pt2.X - pt1.X;
                y = pt2.Y - pt1.Y;
                if (isCtrl)
                {
                    z = pt2.Y - pt1.Y;
                    y = 0;
                    x = 0;
                }
            }
        }
        private void XYZRotacao(out double x, out double y, out double z)
        {
            x = y = z = 0;
            if (rbX.Checked)
                x = (int)nudAngulo.Value;
            else if (rbY.Checked)
                y = (int)nudAngulo.Value;
            else if (rbZ.Checked)
                z = (int)nudAngulo.Value;
            else
            {
                x = (int)nudAngulo.Value;
                y = (int)nudAngulo.Value;
                z = (int)nudAngulo.Value;
            }

            if (Util.DeltaXY(pt1, pt2) < 0)
            {
                x *= -1;
                y *= -1;
                z *= -1;
            }
        }
        private void XYZEscala(out double x, out double y, out double z)
        {
            x = y = z = 0;
            double escala;
            if (Util.DeltaXY(pt1, pt2) > 0)
                escala = 1.02;
            else
                escala = 0.98;
            if (rbX.Checked)
                x = escala;
            else if (rbY.Checked)
                y = escala;
            else if (rbZ.Checked)
                z = escala;
            else
            {
                x = escala;
                y = escala;
                z = escala;
            }
        }

        #endregion
        #region PictureBoxPrincipal/Transformações
        private void pbPrincipal_MouseDown(object sender, MouseEventArgs e)
        {
            isClicked = true;
            pt1 = new Point(e.X, e.Y);
        }
        private void pbPrincipal_MouseUp(object sender, MouseEventArgs e)
        {
            isClicked = false;
        }
        private void pbPrincipal_MouseMove(object sender, MouseEventArgs e)
        {
            timePbPrincipalFim = Environment.TickCount;

            if (timePbPrincipalFim - timePbPrincipalInicio >= timePbPrincipalFator)
            {
                if (objeto == null)
                    return;
                pt2 = new Point(e.X, e.Y);
                if (isClicked)
                {
                    Vector3 c = objeto.getCentro();
                    double x = 0, y = 0, z = 0;


                    switch (ops)
                    {
                        case 1:
                            XYZTranslacao(out x, out y, out z);
                            objeto.translacao(x, y, z);
                            break;
                        case 2:
                            XYZRotacao(out x, out y, out z);
                            objeto.translacao(-c.getX(), -c.getY(), -c.getZ());
                            objeto.rotacao(x, y, z);
                            objeto.translacao(c.getX(), c.getY(), c.getZ());
                            break;
                        case 3:
                            XYZEscala(out x, out y, out z);
                            objeto.translacao(-c.getX(), -c.getY(), -c.getZ());
                            objeto.escala(x, y, z);
                            objeto.translacao(c.getX(), c.getY(), c.getZ());
                            break;
                    }
                    updateTelas(corWire);
                    pt1 = new Point(e.X, e.Y);
                    timePbPrincipalInicio = Environment.TickCount;
                    timePbPrincipalFim = Environment.TickCount;
                }
            }
        }
        private void pbPrincipal_MouseWheel(object sender, MouseEventArgs e)
        {
            if (objeto == null)
                return;
            Vector3 c = objeto.getCentro();
            double escala = 1;
            double x = 1, y = 1, z = 1;

            switch (ops)
            {
                case 2:
                    x = y = z = 0;
                    if (rbX.Checked)
                        x = (int)nudAngulo.Value;
                    else if (rbY.Checked)
                        y = (int)nudAngulo.Value; 
                    else if (rbZ.Checked)
                        z = (int)nudAngulo.Value;
                    else
                    {
                        x = (int)nudAngulo.Value;
                        y = (int)nudAngulo.Value;
                        z = (int)nudAngulo.Value;
                    }
                    if (e.Delta < 0)
                    {
                        x *= -1;
                        y *= -1;
                        z *= -1;
                    }

                    objeto.translacao(-c.getX(), -c.getY(), -c.getZ());
                    objeto.rotacao(x, y, z);
                    objeto.translacao(c.getX(), c.getY(), c.getZ());
                    break;
                case 3:
                    if (e.Delta > 0)
                        escala = 1.5;
                    else
                        escala = 0.5;
                    if (rbX.Checked)
                        x = escala;
                    else if (rbY.Checked)
                        y = escala;
                    else if (rbZ.Checked)
                        z = escala;
                    else
                    {
                        x = escala;
                        y = escala;
                        z = escala;
                    }
                    objeto.translacao(-c.getX(), -c.getY(), -c.getZ());
                    objeto.escala(x, y, z);
                    objeto.translacao(c.getX(), c.getY(), c.getZ());
                    break;
            }
            updateTelas(corWire);
        }
        #endregion
        #region Teclado
        private void FRMPrincipal_KeyPress(object sender, KeyPressEventArgs e)
        {
            switch (e.KeyChar)
            {
                case (char)27:
                    toolStripButtonSeta_Click(null, null);
                    break;
                case 'a':
                    tslEixo.Text = "Todos";
                    rbTodos.Checked = true;
                    break;
                case 'x':
                    tslEixo.Text = "X";
                    rbX.Checked = true;
                    break;
                case 'y':
                    tslEixo.Text = "Y";
                    rbY.Checked = true;
                    break;
                case 'z':
                    tslEixo.Text = "Z";
                    rbZ.Checked = true;
                    break;
                case 'g':
                    toolStripButtonTranslacao_Click(null, null);
                    break;
                case 'r':
                    toolStripButtonRotacao_Click(null, null);
                    break;
                case 's':
                    toolStripButtonEscala_Click(null, null);
                    break;
            }
        }
        private void FRMPrincipal_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control)
                isCtrl = true;
        }
        private void FRMPrincipal_KeyUp(object sender, KeyEventArgs e)
        {
            isCtrl = false;
        }
        #endregion
        #region Atualização Tela
        private int opDesenhaTela ()
        {
            int opDesenha = 1;
            if (rbOrtografica.Checked)
                opDesenha = 1;
            else if (rbGabinete.Checked)
                opDesenha = 2;
            else if (rbCavaleira.Checked)
                opDesenha = 3;
            else if (rbPerspectiva.Checked)
                opDesenha = 4;
            return opDesenha;
        }
        private int opIluminacaoTela()
        {
            int opDesenha = 0;
            if (rbWire.Checked)
                opDesenha = 1;
            else if (rbFlat.Checked)
                opDesenha = 2;
            else if (rbPhong.Checked)
                opDesenha = 3;
            else if (rbGouraud.Checked)
                opDesenha = 4;
            return opDesenha;
        }

        private void cbEnableMiatura_CheckedChanged(object sender, EventArgs e)
        {
            if (((CheckBox)sender).Checked)
                updateTelas(corWire);
            else
                clearPictureBox();
        }
        private void desenhaPictureBox(Color c)
        {
            if (objeto != null)
            {
                switch (opDesenhaTela())
                {
                    case 1: // Ortografica XY Wire
                        objeto.drawOrtografica(imgPrincipal, c,btAmbiente.BackColor, btDifusa.BackColor,btEspecular.BackColor, 1, opIluminacaoTela(),cbFacesOcultas.Checked,luz,(double)nudI.Value,(byte)nudN.Value);
                        break;
                    case 2://Gabinete
                        objeto.drawWireOblique(imgPrincipal, c, btAmbiente.BackColor, btDifusa.BackColor, btEspecular.BackColor, 0.5f, opIluminacaoTela(), cbFacesOcultas.Checked, luz, (double)nudI.Value, (byte)nudN.Value);
                        break;
                    case 3://Cavaleira
                        objeto.drawWireOblique(imgPrincipal, c, btAmbiente.BackColor, btDifusa.BackColor, btEspecular.BackColor, 1.0f, opIluminacaoTela(), cbFacesOcultas.Checked, luz, (double)nudI.Value, (byte)nudN.Value);
                        break;
                    case 4://Perspectiva
                        objeto.drawWirePerspectiva(imgPrincipal, c, btAmbiente.BackColor, btDifusa.BackColor, btEspecular.BackColor, (float)nudD.Value, opIluminacaoTela(), cbFacesOcultas.Checked, luz, (double)nudI.Value, (byte)nudN.Value);
                        break;
                }
            }
            pbPrincipal.Image = imgPrincipal;
            if (cbEnableMiatura.Checked)
            {
                objeto.drawOrtografica(imgXY, c, btAmbiente.BackColor, btDifusa.BackColor, btEspecular.BackColor, 2, opIluminacaoTela(), false, luz, (double)nudI.Value, (byte)nudN.Value);
                objeto.drawOrtografica(imgXZ, c, btAmbiente.BackColor, btDifusa.BackColor, btEspecular.BackColor, 3, opIluminacaoTela(), false, luz, (double)nudI.Value, (byte)nudN.Value);
                objeto.drawOrtografica(imgYZ, c, btAmbiente.BackColor, btDifusa.BackColor, btEspecular.BackColor, 4, opIluminacaoTela(), false, luz, (double)nudI.Value, (byte)nudN.Value);

                pbXY.Image = imgXY;
                pbXZ.Image = imgXZ;
                pbYZ.Image = imgYZ;
            }

        }
        private void clearPictureBox()
        {
            imgPrincipal.Dispose();
            imgXY.Dispose();
            imgXZ.Dispose();
            imgYZ.Dispose();
            imgPrincipal = new Bitmap(getW(), getH());
            imgXY = new Bitmap((int)(getW() * escalaImgW), (int)(getH() * escalaImgH));
            imgXZ = new Bitmap((int)(getW() * escalaImgW), (int)(getH() * escalaImgH));
            imgYZ = new Bitmap((int)(getW() * escalaImgW), (int)(getH() * escalaImgH));
        }

        private void trocaCor(object sender, EventArgs e)
        {
            if (colorDialog.ShowDialog() == DialogResult.OK)
            {
                ((Button)sender).BackColor = colorDialog.Color;
                updateTelas(corWire);
            }
        }

        private void Altera_Modo(object sender, EventArgs e)
        {
            if (objeto == null)
                return;
            updateTelas(corWire);
            objeto.updateVNFaces();
        }

        private void nudD_ValueChanged(object sender, EventArgs e)
        {
            updateTelas(corWire);
        }        

        private void updateTelas(Color c)
        {
            clearPictureBox();
            desenhaPictureBox(c);            
        }

        private void nud_luz(object sender, EventArgs e)
        {
            luz = new Vector3((double)nudX.Value, (double)nudY.Value, (double)nudZ.Value);
            updateTelas(corWire);
        }

        private void Change_Vector3Luz(object sender, EventArgs e)
        {
            luz.setX((double)nudX.Value);
            luz.setY((double)nudY.Value);
            luz.setZ((double)nudZ.Value);
            updateTelas(corWire);
        }
        #endregion
    }
}