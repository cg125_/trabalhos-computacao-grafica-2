﻿namespace _3D.UI
{
    partial class FRMPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.manualToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.tslObjInfo = new System.Windows.Forms.ToolStripStatusLabel();
            this.tslEixo = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStrip = new System.Windows.Forms.ToolStrip();
            this.toolStripButtonCor = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonSeta = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonTranslacao = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonRotacao = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonEscala = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonZoomIn = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonZoomOut = new System.Windows.Forms.ToolStripButton();
            this.toolStripContainer = new System.Windows.Forms.ToolStripContainer();
            this.tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.pbPrincipal = new System.Windows.Forms.PictureBox();
            this.flowLayoutPanelHead = new System.Windows.Forms.FlowLayoutPanel();
            this.groupProjecoes = new System.Windows.Forms.GroupBox();
            this.flowLayoutPanelProjecoes = new System.Windows.Forms.FlowLayoutPanel();
            this.rbPerspectiva = new System.Windows.Forms.RadioButton();
            this.rbOrtografica = new System.Windows.Forms.RadioButton();
            this.rbGabinete = new System.Windows.Forms.RadioButton();
            this.rbCavaleira = new System.Windows.Forms.RadioButton();
            this.gbModo = new System.Windows.Forms.GroupBox();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.rbWire = new System.Windows.Forms.RadioButton();
            this.rbFlat = new System.Windows.Forms.RadioButton();
            this.rbPhong = new System.Windows.Forms.RadioButton();
            this.rbGouraud = new System.Windows.Forms.RadioButton();
            this.groupBoxEixos = new System.Windows.Forms.GroupBox();
            this.flowLayoutPanelEixo = new System.Windows.Forms.FlowLayoutPanel();
            this.rbTodos = new System.Windows.Forms.RadioButton();
            this.rbX = new System.Windows.Forms.RadioButton();
            this.rbY = new System.Windows.Forms.RadioButton();
            this.rbZ = new System.Windows.Forms.RadioButton();
            this.groupBoxVariaveis = new System.Windows.Forms.GroupBox();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.nudAngulo = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.nudD = new System.Windows.Forms.NumericUpDown();
            this.cbEnableMiatura = new System.Windows.Forms.CheckBox();
            this.cbFacesOcultas = new System.Windows.Forms.CheckBox();
            this.pbXZ = new System.Windows.Forms.PictureBox();
            this.pbXY = new System.Windows.Forms.PictureBox();
            this.pbYZ = new System.Windows.Forms.PictureBox();
            this.flowLayoutPanelFoot = new System.Windows.Forms.FlowLayoutPanel();
            this.gbVetorLuz = new System.Windows.Forms.GroupBox();
            this.flpVetorLuz = new System.Windows.Forms.FlowLayoutPanel();
            this.X = new System.Windows.Forms.Label();
            this.nudX = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.nudY = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.nudZ = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.nudI = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.nudN = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.btDifusa = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.btEspecular = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.btAmbiente = new System.Windows.Forms.Button();
            this.colorDialog = new System.Windows.Forms.ColorDialog();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.menuStrip.SuspendLayout();
            this.statusStrip.SuspendLayout();
            this.toolStrip.SuspendLayout();
            this.toolStripContainer.ContentPanel.SuspendLayout();
            this.toolStripContainer.LeftToolStripPanel.SuspendLayout();
            this.toolStripContainer.SuspendLayout();
            this.tableLayoutPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbPrincipal)).BeginInit();
            this.flowLayoutPanelHead.SuspendLayout();
            this.groupProjecoes.SuspendLayout();
            this.flowLayoutPanelProjecoes.SuspendLayout();
            this.gbModo.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.groupBoxEixos.SuspendLayout();
            this.flowLayoutPanelEixo.SuspendLayout();
            this.groupBoxVariaveis.SuspendLayout();
            this.flowLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudAngulo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbXZ)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbXY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbYZ)).BeginInit();
            this.flowLayoutPanelFoot.SuspendLayout();
            this.gbVetorLuz.SuspendLayout();
            this.flpVetorLuz.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudZ)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudI)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudN)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip
            // 
            this.menuStrip.BackColor = System.Drawing.SystemColors.ControlLight;
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(1350, 24);
            this.menuStrip.TabIndex = 2;
            this.menuStrip.Text = "menuStrip";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openToolStripMenuItem,
            this.toolStripSeparator1,
            this.saveToolStripMenuItem,
            this.saveAsToolStripMenuItem,
            this.toolStripSeparator2,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.openToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.openToolStripMenuItem.Text = "Open";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(143, 6);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.saveToolStripMenuItem.Text = "Save";
            // 
            // saveAsToolStripMenuItem
            // 
            this.saveAsToolStripMenuItem.Name = "saveAsToolStripMenuItem";
            this.saveAsToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.saveAsToolStripMenuItem.Text = "Save as";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(143, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.manualToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // manualToolStripMenuItem
            // 
            this.manualToolStripMenuItem.Name = "manualToolStripMenuItem";
            this.manualToolStripMenuItem.Size = new System.Drawing.Size(114, 22);
            this.manualToolStripMenuItem.Text = "Manual";
            this.manualToolStripMenuItem.Click += new System.EventHandler(this.manualToolStripMenuItem_Click);
            // 
            // statusStrip
            // 
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tslObjInfo,
            this.tslEixo});
            this.statusStrip.Location = new System.Drawing.Point(0, 657);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size(1350, 22);
            this.statusStrip.TabIndex = 3;
            this.statusStrip.Text = "statusStrip1";
            // 
            // tslObjInfo
            // 
            this.tslObjInfo.Name = "tslObjInfo";
            this.tslObjInfo.Size = new System.Drawing.Size(95, 17);
            this.tslObjInfo.Text = "Vetores:0 Faces:0";
            // 
            // tslEixo
            // 
            this.tslEixo.Name = "tslEixo";
            this.tslEixo.Size = new System.Drawing.Size(28, 17);
            this.tslEixo.Text = "Eixo";
            // 
            // toolStrip
            // 
            this.toolStrip.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButtonCor,
            this.toolStripSeparator3,
            this.toolStripButtonSeta,
            this.toolStripButtonTranslacao,
            this.toolStripButtonRotacao,
            this.toolStripButtonEscala,
            this.toolStripSeparator4,
            this.toolStripButtonZoomIn,
            this.toolStripButtonZoomOut});
            this.toolStrip.Location = new System.Drawing.Point(0, 3);
            this.toolStrip.Name = "toolStrip";
            this.toolStrip.Size = new System.Drawing.Size(37, 296);
            this.toolStrip.TabIndex = 4;
            this.toolStrip.Text = "toolStrip";
            // 
            // toolStripButtonCor
            // 
            this.toolStripButtonCor.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonCor.Image = global::_3D.Properties.Resources.Paleta_32x32;
            this.toolStripButtonCor.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButtonCor.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonCor.Name = "toolStripButtonCor";
            this.toolStripButtonCor.Size = new System.Drawing.Size(35, 36);
            this.toolStripButtonCor.Text = "ColorCor";
            this.toolStripButtonCor.ToolTipText = "Cor";
            this.toolStripButtonCor.Click += new System.EventHandler(this.toolStripButtonCor_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(35, 6);
            // 
            // toolStripButtonSeta
            // 
            this.toolStripButtonSeta.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonSeta.Image = global::_3D.Properties.Resources.Seta;
            this.toolStripButtonSeta.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButtonSeta.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonSeta.Name = "toolStripButtonSeta";
            this.toolStripButtonSeta.Size = new System.Drawing.Size(35, 36);
            this.toolStripButtonSeta.Text = "toolStripButtonSeta";
            this.toolStripButtonSeta.ToolTipText = "Seta";
            this.toolStripButtonSeta.Click += new System.EventHandler(this.toolStripButtonSeta_Click);
            // 
            // toolStripButtonTranslacao
            // 
            this.toolStripButtonTranslacao.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonTranslacao.Image = global::_3D.Properties.Resources.Translação32x32;
            this.toolStripButtonTranslacao.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButtonTranslacao.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonTranslacao.Name = "toolStripButtonTranslacao";
            this.toolStripButtonTranslacao.Size = new System.Drawing.Size(35, 36);
            this.toolStripButtonTranslacao.Text = "toolStripButtonTranslacao";
            this.toolStripButtonTranslacao.ToolTipText = "Translacao";
            this.toolStripButtonTranslacao.Click += new System.EventHandler(this.toolStripButtonTranslacao_Click);
            // 
            // toolStripButtonRotacao
            // 
            this.toolStripButtonRotacao.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonRotacao.Image = global::_3D.Properties.Resources.Rotação32x32;
            this.toolStripButtonRotacao.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButtonRotacao.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonRotacao.Name = "toolStripButtonRotacao";
            this.toolStripButtonRotacao.Size = new System.Drawing.Size(35, 36);
            this.toolStripButtonRotacao.Text = "toolStripButtonRotacao";
            this.toolStripButtonRotacao.ToolTipText = "Rotacao";
            this.toolStripButtonRotacao.Click += new System.EventHandler(this.toolStripButtonRotacao_Click);
            // 
            // toolStripButtonEscala
            // 
            this.toolStripButtonEscala.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonEscala.Image = global::_3D.Properties.Resources.Escala32x32;
            this.toolStripButtonEscala.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButtonEscala.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonEscala.Name = "toolStripButtonEscala";
            this.toolStripButtonEscala.Size = new System.Drawing.Size(35, 36);
            this.toolStripButtonEscala.Text = "toolStripButtonEscala";
            this.toolStripButtonEscala.ToolTipText = "Escala";
            this.toolStripButtonEscala.Click += new System.EventHandler(this.toolStripButtonEscala_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(35, 6);
            // 
            // toolStripButtonZoomIn
            // 
            this.toolStripButtonZoomIn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonZoomIn.Image = global::_3D.Properties.Resources.Zoom_In_32x32;
            this.toolStripButtonZoomIn.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButtonZoomIn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonZoomIn.Name = "toolStripButtonZoomIn";
            this.toolStripButtonZoomIn.Size = new System.Drawing.Size(35, 36);
            this.toolStripButtonZoomIn.Text = "Zoom In";
            this.toolStripButtonZoomIn.Click += new System.EventHandler(this.toolStripButtonZoomIn_Click);
            // 
            // toolStripButtonZoomOut
            // 
            this.toolStripButtonZoomOut.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonZoomOut.Image = global::_3D.Properties.Resources.Zoom_out_32x32;
            this.toolStripButtonZoomOut.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButtonZoomOut.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonZoomOut.Name = "toolStripButtonZoomOut";
            this.toolStripButtonZoomOut.Size = new System.Drawing.Size(35, 36);
            this.toolStripButtonZoomOut.Text = "Zoom out";
            this.toolStripButtonZoomOut.ToolTipText = "Zoom Out";
            this.toolStripButtonZoomOut.Click += new System.EventHandler(this.toolStripButtonZoomOut_Click);
            // 
            // toolStripContainer
            // 
            // 
            // toolStripContainer.ContentPanel
            // 
            this.toolStripContainer.ContentPanel.Controls.Add(this.tableLayoutPanel);
            this.toolStripContainer.ContentPanel.Size = new System.Drawing.Size(1313, 608);
            this.toolStripContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            // 
            // toolStripContainer.LeftToolStripPanel
            // 
            this.toolStripContainer.LeftToolStripPanel.Controls.Add(this.toolStrip);
            this.toolStripContainer.Location = new System.Drawing.Point(0, 24);
            this.toolStripContainer.Name = "toolStripContainer";
            this.toolStripContainer.Size = new System.Drawing.Size(1350, 633);
            this.toolStripContainer.TabIndex = 5;
            this.toolStripContainer.Text = "toolStripContainer1";
            // 
            // tableLayoutPanel
            // 
            this.tableLayoutPanel.ColumnCount = 2;
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 400F));
            this.tableLayoutPanel.Controls.Add(this.pbPrincipal, 0, 1);
            this.tableLayoutPanel.Controls.Add(this.flowLayoutPanelHead, 0, 0);
            this.tableLayoutPanel.Controls.Add(this.pbXZ, 1, 2);
            this.tableLayoutPanel.Controls.Add(this.pbXY, 1, 1);
            this.tableLayoutPanel.Controls.Add(this.pbYZ, 1, 3);
            this.tableLayoutPanel.Controls.Add(this.flowLayoutPanelFoot, 0, 4);
            this.tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel.Name = "tableLayoutPanel";
            this.tableLayoutPanel.RowCount = 5;
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 64F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 64F));
            this.tableLayoutPanel.Size = new System.Drawing.Size(1313, 608);
            this.tableLayoutPanel.TabIndex = 1;
            // 
            // pbPrincipal
            // 
            this.pbPrincipal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pbPrincipal.Location = new System.Drawing.Point(3, 67);
            this.pbPrincipal.Name = "pbPrincipal";
            this.tableLayoutPanel.SetRowSpan(this.pbPrincipal, 3);
            this.pbPrincipal.Size = new System.Drawing.Size(907, 474);
            this.pbPrincipal.TabIndex = 3;
            this.pbPrincipal.TabStop = false;
            this.pbPrincipal.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pbPrincipal_MouseDown);
            this.pbPrincipal.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pbPrincipal_MouseMove);
            this.pbPrincipal.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pbPrincipal_MouseUp);
            this.pbPrincipal.MouseWheel += new System.Windows.Forms.MouseEventHandler(this.pbPrincipal_MouseWheel);
            // 
            // flowLayoutPanelHead
            // 
            this.tableLayoutPanel.SetColumnSpan(this.flowLayoutPanelHead, 2);
            this.flowLayoutPanelHead.Controls.Add(this.groupProjecoes);
            this.flowLayoutPanelHead.Controls.Add(this.gbModo);
            this.flowLayoutPanelHead.Controls.Add(this.groupBoxEixos);
            this.flowLayoutPanelHead.Controls.Add(this.groupBoxVariaveis);
            this.flowLayoutPanelHead.Controls.Add(this.cbEnableMiatura);
            this.flowLayoutPanelHead.Controls.Add(this.cbFacesOcultas);
            this.flowLayoutPanelHead.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanelHead.Location = new System.Drawing.Point(0, 3);
            this.flowLayoutPanelHead.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.flowLayoutPanelHead.Name = "flowLayoutPanelHead";
            this.flowLayoutPanelHead.Size = new System.Drawing.Size(1313, 58);
            this.flowLayoutPanelHead.TabIndex = 1;
            // 
            // groupProjecoes
            // 
            this.groupProjecoes.Controls.Add(this.flowLayoutPanelProjecoes);
            this.groupProjecoes.Location = new System.Drawing.Point(3, 3);
            this.groupProjecoes.Name = "groupProjecoes";
            this.groupProjecoes.Size = new System.Drawing.Size(331, 55);
            this.groupProjecoes.TabIndex = 0;
            this.groupProjecoes.TabStop = false;
            this.groupProjecoes.Text = "Projeções";
            // 
            // flowLayoutPanelProjecoes
            // 
            this.flowLayoutPanelProjecoes.Controls.Add(this.rbPerspectiva);
            this.flowLayoutPanelProjecoes.Controls.Add(this.rbOrtografica);
            this.flowLayoutPanelProjecoes.Controls.Add(this.rbGabinete);
            this.flowLayoutPanelProjecoes.Controls.Add(this.rbCavaleira);
            this.flowLayoutPanelProjecoes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanelProjecoes.Location = new System.Drawing.Point(3, 16);
            this.flowLayoutPanelProjecoes.Name = "flowLayoutPanelProjecoes";
            this.flowLayoutPanelProjecoes.Size = new System.Drawing.Size(325, 36);
            this.flowLayoutPanelProjecoes.TabIndex = 0;
            // 
            // rbPerspectiva
            // 
            this.rbPerspectiva.AutoSize = true;
            this.rbPerspectiva.Checked = true;
            this.rbPerspectiva.Location = new System.Drawing.Point(3, 3);
            this.rbPerspectiva.Name = "rbPerspectiva";
            this.rbPerspectiva.Padding = new System.Windows.Forms.Padding(0, 5, 0, 0);
            this.rbPerspectiva.Size = new System.Drawing.Size(84, 22);
            this.rbPerspectiva.TabIndex = 7;
            this.rbPerspectiva.TabStop = true;
            this.rbPerspectiva.Text = "Perspectiva ";
            this.rbPerspectiva.UseVisualStyleBackColor = true;
            this.rbPerspectiva.CheckedChanged += new System.EventHandler(this.Altera_Modo);
            // 
            // rbOrtografica
            // 
            this.rbOrtografica.AutoSize = true;
            this.rbOrtografica.Location = new System.Drawing.Point(93, 3);
            this.rbOrtografica.Name = "rbOrtografica";
            this.rbOrtografica.Padding = new System.Windows.Forms.Padding(0, 5, 0, 0);
            this.rbOrtografica.Size = new System.Drawing.Size(77, 22);
            this.rbOrtografica.TabIndex = 8;
            this.rbOrtografica.Text = "Ortográfica";
            this.rbOrtografica.UseVisualStyleBackColor = true;
            this.rbOrtografica.CheckedChanged += new System.EventHandler(this.Altera_Modo);
            // 
            // rbGabinete
            // 
            this.rbGabinete.AutoSize = true;
            this.rbGabinete.Location = new System.Drawing.Point(176, 3);
            this.rbGabinete.Name = "rbGabinete";
            this.rbGabinete.Padding = new System.Windows.Forms.Padding(0, 5, 0, 0);
            this.rbGabinete.Size = new System.Drawing.Size(68, 22);
            this.rbGabinete.TabIndex = 5;
            this.rbGabinete.Text = "Gabinete";
            this.rbGabinete.UseVisualStyleBackColor = true;
            this.rbGabinete.CheckedChanged += new System.EventHandler(this.Altera_Modo);
            // 
            // rbCavaleira
            // 
            this.rbCavaleira.AutoSize = true;
            this.rbCavaleira.Location = new System.Drawing.Point(250, 3);
            this.rbCavaleira.Name = "rbCavaleira";
            this.rbCavaleira.Padding = new System.Windows.Forms.Padding(0, 5, 0, 0);
            this.rbCavaleira.Size = new System.Drawing.Size(69, 22);
            this.rbCavaleira.TabIndex = 6;
            this.rbCavaleira.Text = "Cavaleira";
            this.rbCavaleira.UseVisualStyleBackColor = true;
            this.rbCavaleira.CheckedChanged += new System.EventHandler(this.Altera_Modo);
            // 
            // gbModo
            // 
            this.gbModo.Controls.Add(this.flowLayoutPanel1);
            this.gbModo.Location = new System.Drawing.Point(340, 3);
            this.gbModo.Name = "gbModo";
            this.gbModo.Size = new System.Drawing.Size(255, 55);
            this.gbModo.TabIndex = 6;
            this.gbModo.TabStop = false;
            this.gbModo.Text = "Modo";
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.rbWire);
            this.flowLayoutPanel1.Controls.Add(this.rbFlat);
            this.flowLayoutPanel1.Controls.Add(this.rbPhong);
            this.flowLayoutPanel1.Controls.Add(this.rbGouraud);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(3, 16);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(249, 36);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // rbWire
            // 
            this.rbWire.AutoSize = true;
            this.rbWire.Checked = true;
            this.rbWire.Location = new System.Drawing.Point(3, 3);
            this.rbWire.Name = "rbWire";
            this.rbWire.Padding = new System.Windows.Forms.Padding(0, 5, 0, 0);
            this.rbWire.Size = new System.Drawing.Size(47, 22);
            this.rbWire.TabIndex = 3;
            this.rbWire.TabStop = true;
            this.rbWire.Text = "Wire";
            this.rbWire.UseVisualStyleBackColor = true;
            this.rbWire.CheckedChanged += new System.EventHandler(this.Altera_Modo);
            // 
            // rbFlat
            // 
            this.rbFlat.AutoSize = true;
            this.rbFlat.Location = new System.Drawing.Point(56, 3);
            this.rbFlat.Name = "rbFlat";
            this.rbFlat.Padding = new System.Windows.Forms.Padding(0, 5, 0, 0);
            this.rbFlat.Size = new System.Drawing.Size(42, 22);
            this.rbFlat.TabIndex = 0;
            this.rbFlat.Text = "Flat";
            this.rbFlat.UseVisualStyleBackColor = true;
            this.rbFlat.CheckedChanged += new System.EventHandler(this.Altera_Modo);
            // 
            // rbPhong
            // 
            this.rbPhong.AutoSize = true;
            this.rbPhong.Location = new System.Drawing.Point(104, 3);
            this.rbPhong.Name = "rbPhong";
            this.rbPhong.Padding = new System.Windows.Forms.Padding(0, 5, 0, 0);
            this.rbPhong.Size = new System.Drawing.Size(56, 22);
            this.rbPhong.TabIndex = 1;
            this.rbPhong.Text = "Phong";
            this.rbPhong.UseVisualStyleBackColor = true;
            this.rbPhong.CheckedChanged += new System.EventHandler(this.Altera_Modo);
            // 
            // rbGouraud
            // 
            this.rbGouraud.AutoSize = true;
            this.rbGouraud.Location = new System.Drawing.Point(166, 3);
            this.rbGouraud.Name = "rbGouraud";
            this.rbGouraud.Padding = new System.Windows.Forms.Padding(0, 5, 0, 0);
            this.rbGouraud.Size = new System.Drawing.Size(66, 22);
            this.rbGouraud.TabIndex = 2;
            this.rbGouraud.Text = "Gouraud";
            this.rbGouraud.UseVisualStyleBackColor = true;
            this.rbGouraud.CheckedChanged += new System.EventHandler(this.Altera_Modo);
            // 
            // groupBoxEixos
            // 
            this.groupBoxEixos.Controls.Add(this.flowLayoutPanelEixo);
            this.groupBoxEixos.Location = new System.Drawing.Point(601, 3);
            this.groupBoxEixos.Name = "groupBoxEixos";
            this.groupBoxEixos.Size = new System.Drawing.Size(187, 55);
            this.groupBoxEixos.TabIndex = 1;
            this.groupBoxEixos.TabStop = false;
            this.groupBoxEixos.Text = "Eixo";
            // 
            // flowLayoutPanelEixo
            // 
            this.flowLayoutPanelEixo.Controls.Add(this.rbTodos);
            this.flowLayoutPanelEixo.Controls.Add(this.rbX);
            this.flowLayoutPanelEixo.Controls.Add(this.rbY);
            this.flowLayoutPanelEixo.Controls.Add(this.rbZ);
            this.flowLayoutPanelEixo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanelEixo.Location = new System.Drawing.Point(3, 16);
            this.flowLayoutPanelEixo.Name = "flowLayoutPanelEixo";
            this.flowLayoutPanelEixo.Size = new System.Drawing.Size(181, 36);
            this.flowLayoutPanelEixo.TabIndex = 0;
            // 
            // rbTodos
            // 
            this.rbTodos.AutoSize = true;
            this.rbTodos.Checked = true;
            this.rbTodos.Location = new System.Drawing.Point(3, 3);
            this.rbTodos.Name = "rbTodos";
            this.rbTodos.Padding = new System.Windows.Forms.Padding(0, 5, 0, 0);
            this.rbTodos.Size = new System.Drawing.Size(55, 22);
            this.rbTodos.TabIndex = 3;
            this.rbTodos.TabStop = true;
            this.rbTodos.Text = "Todos";
            this.rbTodos.UseVisualStyleBackColor = true;
            // 
            // rbX
            // 
            this.rbX.AutoSize = true;
            this.rbX.Location = new System.Drawing.Point(64, 3);
            this.rbX.Name = "rbX";
            this.rbX.Padding = new System.Windows.Forms.Padding(0, 5, 0, 0);
            this.rbX.Size = new System.Drawing.Size(32, 22);
            this.rbX.TabIndex = 0;
            this.rbX.Text = "X";
            this.rbX.UseVisualStyleBackColor = true;
            // 
            // rbY
            // 
            this.rbY.AutoSize = true;
            this.rbY.Location = new System.Drawing.Point(102, 3);
            this.rbY.Name = "rbY";
            this.rbY.Padding = new System.Windows.Forms.Padding(0, 5, 0, 0);
            this.rbY.Size = new System.Drawing.Size(32, 22);
            this.rbY.TabIndex = 1;
            this.rbY.Text = "Y";
            this.rbY.UseVisualStyleBackColor = true;
            // 
            // rbZ
            // 
            this.rbZ.AutoSize = true;
            this.rbZ.Location = new System.Drawing.Point(140, 3);
            this.rbZ.Name = "rbZ";
            this.rbZ.Padding = new System.Windows.Forms.Padding(0, 5, 0, 0);
            this.rbZ.Size = new System.Drawing.Size(32, 22);
            this.rbZ.TabIndex = 2;
            this.rbZ.Text = "Z";
            this.rbZ.UseVisualStyleBackColor = true;
            // 
            // groupBoxVariaveis
            // 
            this.groupBoxVariaveis.Controls.Add(this.flowLayoutPanel2);
            this.groupBoxVariaveis.Location = new System.Drawing.Point(794, 3);
            this.groupBoxVariaveis.Name = "groupBoxVariaveis";
            this.groupBoxVariaveis.Size = new System.Drawing.Size(193, 55);
            this.groupBoxVariaveis.TabIndex = 3;
            this.groupBoxVariaveis.TabStop = false;
            this.groupBoxVariaveis.Text = "Rotação/Perspectiva";
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Controls.Add(this.nudAngulo);
            this.flowLayoutPanel2.Controls.Add(this.label1);
            this.flowLayoutPanel2.Controls.Add(this.label2);
            this.flowLayoutPanel2.Controls.Add(this.nudD);
            this.flowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(3, 16);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(187, 36);
            this.flowLayoutPanel2.TabIndex = 0;
            // 
            // nudAngulo
            // 
            this.nudAngulo.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.nudAngulo.Location = new System.Drawing.Point(3, 5);
            this.nudAngulo.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this.nudAngulo.Name = "nudAngulo";
            this.nudAngulo.Size = new System.Drawing.Size(56, 20);
            this.nudAngulo.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(62, 3);
            this.label1.Margin = new System.Windows.Forms.Padding(0, 3, 3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(11, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "º";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(79, 8);
            this.label2.Margin = new System.Windows.Forms.Padding(3, 8, 3, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(15, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "D";
            // 
            // nudD
            // 
            this.nudD.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.nudD.Location = new System.Drawing.Point(100, 5);
            this.nudD.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this.nudD.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.nudD.Minimum = new decimal(new int[] {
            1000000,
            0,
            0,
            -2147483648});
            this.nudD.Name = "nudD";
            this.nudD.Size = new System.Drawing.Size(70, 20);
            this.nudD.TabIndex = 6;
            this.nudD.Value = new decimal(new int[] {
            1000,
            0,
            0,
            -2147483648});
            this.nudD.ValueChanged += new System.EventHandler(this.nudD_ValueChanged);
            // 
            // cbEnableMiatura
            // 
            this.cbEnableMiatura.AutoSize = true;
            this.cbEnableMiatura.Location = new System.Drawing.Point(993, 25);
            this.cbEnableMiatura.Margin = new System.Windows.Forms.Padding(3, 25, 3, 3);
            this.cbEnableMiatura.Name = "cbEnableMiatura";
            this.cbEnableMiatura.Size = new System.Drawing.Size(74, 17);
            this.cbEnableMiatura.TabIndex = 4;
            this.cbEnableMiatura.Text = "Miniaturas";
            this.cbEnableMiatura.UseVisualStyleBackColor = true;
            this.cbEnableMiatura.CheckedChanged += new System.EventHandler(this.cbEnableMiatura_CheckedChanged);
            // 
            // cbFacesOcultas
            // 
            this.cbFacesOcultas.AutoSize = true;
            this.cbFacesOcultas.Checked = true;
            this.cbFacesOcultas.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbFacesOcultas.Location = new System.Drawing.Point(1073, 25);
            this.cbFacesOcultas.Margin = new System.Windows.Forms.Padding(3, 25, 3, 3);
            this.cbFacesOcultas.Name = "cbFacesOcultas";
            this.cbFacesOcultas.Size = new System.Drawing.Size(92, 17);
            this.cbFacesOcultas.TabIndex = 5;
            this.cbFacesOcultas.Text = "Faces Ocultar";
            this.cbFacesOcultas.UseVisualStyleBackColor = true;
            this.cbFacesOcultas.CheckedChanged += new System.EventHandler(this.Altera_Modo);
            // 
            // pbXZ
            // 
            this.pbXZ.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pbXZ.Location = new System.Drawing.Point(916, 227);
            this.pbXZ.Name = "pbXZ";
            this.pbXZ.Size = new System.Drawing.Size(394, 154);
            this.pbXZ.TabIndex = 1;
            this.pbXZ.TabStop = false;
            // 
            // pbXY
            // 
            this.pbXY.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pbXY.Location = new System.Drawing.Point(916, 67);
            this.pbXY.Name = "pbXY";
            this.pbXY.Size = new System.Drawing.Size(394, 154);
            this.pbXY.TabIndex = 4;
            this.pbXY.TabStop = false;
            // 
            // pbYZ
            // 
            this.pbYZ.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pbYZ.Location = new System.Drawing.Point(916, 387);
            this.pbYZ.Name = "pbYZ";
            this.pbYZ.Size = new System.Drawing.Size(394, 154);
            this.pbYZ.TabIndex = 2;
            this.pbYZ.TabStop = false;
            // 
            // flowLayoutPanelFoot
            // 
            this.tableLayoutPanel.SetColumnSpan(this.flowLayoutPanelFoot, 2);
            this.flowLayoutPanelFoot.Controls.Add(this.gbVetorLuz);
            this.flowLayoutPanelFoot.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanelFoot.Location = new System.Drawing.Point(3, 547);
            this.flowLayoutPanelFoot.Name = "flowLayoutPanelFoot";
            this.flowLayoutPanelFoot.Size = new System.Drawing.Size(1307, 58);
            this.flowLayoutPanelFoot.TabIndex = 5;
            // 
            // gbVetorLuz
            // 
            this.gbVetorLuz.Controls.Add(this.flpVetorLuz);
            this.gbVetorLuz.Location = new System.Drawing.Point(3, 3);
            this.gbVetorLuz.Name = "gbVetorLuz";
            this.gbVetorLuz.Padding = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.gbVetorLuz.Size = new System.Drawing.Size(823, 55);
            this.gbVetorLuz.TabIndex = 8;
            this.gbVetorLuz.TabStop = false;
            this.gbVetorLuz.Text = "Luz";
            // 
            // flpVetorLuz
            // 
            this.flpVetorLuz.Controls.Add(this.X);
            this.flpVetorLuz.Controls.Add(this.nudX);
            this.flpVetorLuz.Controls.Add(this.label3);
            this.flpVetorLuz.Controls.Add(this.nudY);
            this.flpVetorLuz.Controls.Add(this.label4);
            this.flpVetorLuz.Controls.Add(this.nudZ);
            this.flpVetorLuz.Controls.Add(this.label5);
            this.flpVetorLuz.Controls.Add(this.nudI);
            this.flpVetorLuz.Controls.Add(this.label6);
            this.flpVetorLuz.Controls.Add(this.nudN);
            this.flpVetorLuz.Controls.Add(this.label7);
            this.flpVetorLuz.Controls.Add(this.btDifusa);
            this.flpVetorLuz.Controls.Add(this.label8);
            this.flpVetorLuz.Controls.Add(this.btEspecular);
            this.flpVetorLuz.Controls.Add(this.label9);
            this.flpVetorLuz.Controls.Add(this.btAmbiente);
            this.flpVetorLuz.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpVetorLuz.Location = new System.Drawing.Point(1, 16);
            this.flpVetorLuz.Name = "flpVetorLuz";
            this.flpVetorLuz.Size = new System.Drawing.Size(821, 36);
            this.flpVetorLuz.TabIndex = 0;
            // 
            // X
            // 
            this.X.AutoSize = true;
            this.X.Location = new System.Drawing.Point(3, 8);
            this.X.Margin = new System.Windows.Forms.Padding(3, 8, 3, 0);
            this.X.Name = "X";
            this.X.Size = new System.Drawing.Size(14, 13);
            this.X.TabIndex = 3;
            this.X.Text = "X";
            // 
            // nudX
            // 
            this.nudX.Location = new System.Drawing.Point(23, 5);
            this.nudX.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this.nudX.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.nudX.Minimum = new decimal(new int[] {
            10000,
            0,
            0,
            -2147483648});
            this.nudX.Name = "nudX";
            this.nudX.Size = new System.Drawing.Size(56, 20);
            this.nudX.TabIndex = 0;
            this.nudX.Value = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.nudX.ValueChanged += new System.EventHandler(this.nud_luz);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(85, 8);
            this.label3.Margin = new System.Windows.Forms.Padding(3, 8, 3, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(14, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Y";
            // 
            // nudY
            // 
            this.nudY.Location = new System.Drawing.Point(105, 5);
            this.nudY.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this.nudY.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.nudY.Minimum = new decimal(new int[] {
            10000,
            0,
            0,
            -2147483648});
            this.nudY.Name = "nudY";
            this.nudY.Size = new System.Drawing.Size(56, 20);
            this.nudY.TabIndex = 1;
            this.nudY.ValueChanged += new System.EventHandler(this.nud_luz);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(167, 8);
            this.label4.Margin = new System.Windows.Forms.Padding(3, 8, 3, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(14, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "Z";
            // 
            // nudZ
            // 
            this.nudZ.Location = new System.Drawing.Point(187, 5);
            this.nudZ.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this.nudZ.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.nudZ.Minimum = new decimal(new int[] {
            10000,
            0,
            0,
            -2147483648});
            this.nudZ.Name = "nudZ";
            this.nudZ.Size = new System.Drawing.Size(56, 20);
            this.nudZ.TabIndex = 2;
            this.nudZ.ValueChanged += new System.EventHandler(this.nud_luz);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(249, 8);
            this.label5.Margin = new System.Windows.Forms.Padding(3, 8, 3, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(10, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "I";
            // 
            // nudI
            // 
            this.nudI.DecimalPlaces = 2;
            this.nudI.Increment = new decimal(new int[] {
            5,
            0,
            0,
            131072});
            this.nudI.Location = new System.Drawing.Point(265, 5);
            this.nudI.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this.nudI.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.nudI.Name = "nudI";
            this.nudI.Size = new System.Drawing.Size(59, 20);
            this.nudI.TabIndex = 6;
            this.nudI.Value = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            this.nudI.ValueChanged += new System.EventHandler(this.nudD_ValueChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(330, 8);
            this.label6.Margin = new System.Windows.Forms.Padding(3, 8, 3, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(15, 13);
            this.label6.TabIndex = 9;
            this.label6.Text = "N";
            // 
            // nudN
            // 
            this.nudN.Location = new System.Drawing.Point(351, 5);
            this.nudN.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this.nudN.Minimum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.nudN.Name = "nudN";
            this.nudN.Size = new System.Drawing.Size(51, 20);
            this.nudN.TabIndex = 8;
            this.nudN.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.nudN.ValueChanged += new System.EventHandler(this.nudD_ValueChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(408, 8);
            this.label7.Margin = new System.Windows.Forms.Padding(3, 8, 3, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(37, 13);
            this.label7.TabIndex = 11;
            this.label7.Text = "Difusa";
            // 
            // btDifusa
            // 
            this.btDifusa.BackColor = System.Drawing.Color.Blue;
            this.btDifusa.Location = new System.Drawing.Point(451, 3);
            this.btDifusa.Name = "btDifusa";
            this.btDifusa.Size = new System.Drawing.Size(75, 23);
            this.btDifusa.TabIndex = 10;
            this.btDifusa.UseVisualStyleBackColor = false;
            this.btDifusa.Click += new System.EventHandler(this.trocaCor);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(532, 8);
            this.label8.Margin = new System.Windows.Forms.Padding(3, 8, 3, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(54, 13);
            this.label8.TabIndex = 13;
            this.label8.Text = "Especular";
            // 
            // btEspecular
            // 
            this.btEspecular.BackColor = System.Drawing.Color.White;
            this.btEspecular.Location = new System.Drawing.Point(592, 3);
            this.btEspecular.Name = "btEspecular";
            this.btEspecular.Size = new System.Drawing.Size(75, 23);
            this.btEspecular.TabIndex = 12;
            this.btEspecular.UseVisualStyleBackColor = false;
            this.btEspecular.Click += new System.EventHandler(this.trocaCor);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(673, 8);
            this.label9.Margin = new System.Windows.Forms.Padding(3, 8, 3, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(51, 13);
            this.label9.TabIndex = 15;
            this.label9.Text = "Ambiente";
            // 
            // btAmbiente
            // 
            this.btAmbiente.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btAmbiente.Location = new System.Drawing.Point(730, 3);
            this.btAmbiente.Name = "btAmbiente";
            this.btAmbiente.Size = new System.Drawing.Size(75, 23);
            this.btAmbiente.TabIndex = 14;
            this.btAmbiente.UseVisualStyleBackColor = false;
            this.btAmbiente.Click += new System.EventHandler(this.trocaCor);
            // 
            // colorDialog
            // 
            this.colorDialog.Color = System.Drawing.Color.White;
            this.colorDialog.FullOpen = true;
            // 
            // openFileDialog
            // 
            this.openFileDialog.FileName = "openFileDialog";
            this.openFileDialog.Filter = "Modelo 3D|*.obj";
            // 
            // FRMPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1350, 679);
            this.Controls.Add(this.toolStripContainer);
            this.Controls.Add(this.statusStrip);
            this.Controls.Add(this.menuStrip);
            this.KeyPreview = true;
            this.MinimumSize = new System.Drawing.Size(1364, 718);
            this.Name = "FRMPrincipal";
            this.Text = "FRMPrincipal";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FRMPrincipal_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FRMPrincipal_KeyDown);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FRMPrincipal_KeyPress);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.FRMPrincipal_KeyUp);
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.toolStrip.ResumeLayout(false);
            this.toolStrip.PerformLayout();
            this.toolStripContainer.ContentPanel.ResumeLayout(false);
            this.toolStripContainer.LeftToolStripPanel.ResumeLayout(false);
            this.toolStripContainer.LeftToolStripPanel.PerformLayout();
            this.toolStripContainer.ResumeLayout(false);
            this.toolStripContainer.PerformLayout();
            this.tableLayoutPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbPrincipal)).EndInit();
            this.flowLayoutPanelHead.ResumeLayout(false);
            this.flowLayoutPanelHead.PerformLayout();
            this.groupProjecoes.ResumeLayout(false);
            this.flowLayoutPanelProjecoes.ResumeLayout(false);
            this.flowLayoutPanelProjecoes.PerformLayout();
            this.gbModo.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            this.groupBoxEixos.ResumeLayout(false);
            this.flowLayoutPanelEixo.ResumeLayout(false);
            this.flowLayoutPanelEixo.PerformLayout();
            this.groupBoxVariaveis.ResumeLayout(false);
            this.flowLayoutPanel2.ResumeLayout(false);
            this.flowLayoutPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudAngulo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbXZ)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbXY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbYZ)).EndInit();
            this.flowLayoutPanelFoot.ResumeLayout(false);
            this.gbVetorLuz.ResumeLayout(false);
            this.flpVetorLuz.ResumeLayout(false);
            this.flpVetorLuz.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudZ)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudI)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudN)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveAsToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem manualToolStripMenuItem;
        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.ToolStripStatusLabel tslObjInfo;
        private System.Windows.Forms.ToolStripStatusLabel tslEixo;
        private System.Windows.Forms.ToolStrip toolStrip;
        private System.Windows.Forms.ToolStripButton toolStripButtonCor;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton toolStripButtonSeta;
        private System.Windows.Forms.ToolStripButton toolStripButtonTranslacao;
        private System.Windows.Forms.ToolStripButton toolStripButtonRotacao;
        private System.Windows.Forms.ToolStripButton toolStripButtonEscala;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripButton toolStripButtonZoomIn;
        private System.Windows.Forms.ToolStripButton toolStripButtonZoomOut;
        private System.Windows.Forms.ToolStripContainer toolStripContainer;
        private System.Windows.Forms.ColorDialog colorDialog;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel;
        private System.Windows.Forms.PictureBox pbPrincipal;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanelHead;
        private System.Windows.Forms.GroupBox groupProjecoes;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanelProjecoes;
        private System.Windows.Forms.RadioButton rbOrtografica;
        private System.Windows.Forms.RadioButton rbGabinete;
        private System.Windows.Forms.RadioButton rbCavaleira;
        private System.Windows.Forms.RadioButton rbPerspectiva;
        private System.Windows.Forms.GroupBox groupBoxEixos;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanelEixo;
        private System.Windows.Forms.RadioButton rbTodos;
        private System.Windows.Forms.RadioButton rbX;
        private System.Windows.Forms.RadioButton rbY;
        private System.Windows.Forms.RadioButton rbZ;
        private System.Windows.Forms.GroupBox groupBoxVariaveis;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.NumericUpDown nudAngulo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown nudD;
        private System.Windows.Forms.PictureBox pbXZ;
        private System.Windows.Forms.PictureBox pbXY;
        private System.Windows.Forms.PictureBox pbYZ;
        private System.Windows.Forms.CheckBox cbEnableMiatura;
        private System.Windows.Forms.CheckBox cbFacesOcultas;
        private System.Windows.Forms.GroupBox gbModo;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.RadioButton rbWire;
        private System.Windows.Forms.RadioButton rbFlat;
        private System.Windows.Forms.RadioButton rbPhong;
        private System.Windows.Forms.RadioButton rbGouraud;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanelFoot;
        private System.Windows.Forms.GroupBox gbVetorLuz;
        private System.Windows.Forms.FlowLayoutPanel flpVetorLuz;
        private System.Windows.Forms.Label X;
        private System.Windows.Forms.NumericUpDown nudX;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown nudY;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown nudZ;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown nudI;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown nudN;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btDifusa;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btEspecular;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button btAmbiente;
    }
}