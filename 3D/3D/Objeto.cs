﻿using _3D.UI;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;

namespace _3D
{
    class Objeto
    {
        private List<Vector3> pontosOriginais;
        private List<Vector3> pontosAtuais;
        private List<Vector3> vn_pts;
        private List<Vector3> vn_faces;
        private List<Face> faces;
        private double[,] matAc;
        private double escalaW,escalaH;

        #region Construtor e get
        public Objeto(List<Vector3> pontos, List<Face> faces,double escalaW,double escalaH)
        {
            this.pontosOriginais = pontos;
            this.faces = faces;
            this.escalaW = escalaW;
            this.escalaH = escalaH;

            pontosAtuais = new List<Vector3>();
            vn_faces = new List<Vector3>();
            vn_pts = new List<Vector3>();
            foreach (Vector3 p in pontos)
            {
                pontosAtuais.Add(new Vector3(p.getX(), p.getY(), p.getZ()));
                vn_pts.Add(new Vector3());
            }    
            foreach (Face f in faces)
                vn_faces.Add(new Vector3());
            matAc = new double[4, 4];
            for (int i = 0; i < 4; i++)
            {
                matAc[i, i] = 1;
            }

            updateVNFaces();
            updateVNVertice_carrega(pontosAtuais);
        }
        public Vector3 getCentro()
        {
            double x = 0, y = 0, z = 0;
            foreach (Vector3 p in pontosAtuais)
            {
                x += p.getX();
                y += p.getY();
                z += p.getZ();
            }

            x /= pontosAtuais.Count;
            y /= pontosAtuais.Count;
            z /= pontosAtuais.Count;

            return new Vector3(x, y, z);
        }
        #endregion
        #region Vetores Normais
        public void updateVNFaces()
        {
            for(int q = 0; q < faces.Count;q++)
            {
                Face f = faces[q];
                Vector3 A = pontosAtuais[f.getPontos()[0]];
                Vector3 B = pontosAtuais[f.getPontos()[1]];
                Vector3 C = pontosAtuais[f.getPontos()[2]];

                Vector3 AB = new Vector3(B.getX() - A.getX(), B.getY() - A.getY(), B.getZ() - A.getZ());
                Vector3 AC = new Vector3(C.getX() - A.getX(), C.getY() - A.getY(), C.getZ() - A.getZ());
                
                double i = AB.getY() * AC.getZ() - AB.getZ() * AC.getY();
                double j = AB.getZ() * AC.getX() - AB.getX() * AC.getZ();
                double k = AB.getX() * AC.getY() - AB.getY() * AC.getX();
                
                Vector3 vn_face = new Vector3(i,j,k);
                vn_face.normalize();
                vn_faces[q] = vn_face;
            }
        }
        private void updateVNFaces(List<Vector3> pts)
        {
            for (int q = 0; q < faces.Count; q++)
            {
                Face f = faces[q];
                Vector3 A = pts[f.getPontos()[0]];
                Vector3 B = pts[f.getPontos()[1]];
                Vector3 C = pts[f.getPontos()[2]];

                A.setZ(pontosAtuais[f.getPontos()[0]].getZ());
                B.setZ(pontosAtuais[f.getPontos()[1]].getZ());
                C.setZ(pontosAtuais[f.getPontos()[2]].getZ());

                Vector3 AB = new Vector3(B.getX() - A.getX(), B.getY() - A.getY(), B.getZ() - A.getZ());
                Vector3 AC = new Vector3(C.getX() - A.getX(), C.getY() - A.getY(), C.getZ() - A.getZ());

                double i = AB.getY() * AC.getZ() - AB.getZ() * AC.getY();
                double j = AB.getZ() * AC.getX() - AB.getX() * AC.getZ();
                double k = AB.getX() * AC.getY() - AB.getY() * AC.getX();

                Vector3 vn_face = new Vector3(i, j, k);
                vn_face.normalize();
                vn_faces[q] = vn_face;
            }
        }
        private void updateVNVertice_carrega(List<Vector3> pts)
        {
            for (int i = 0; i < pts.Count;i++)
            {
                Vector3 p = pts[i];
                List<Vector3> vnFaces = new List<Vector3>();
                for (int j = 0; j < faces.Count; j++)
                {
                    Face f = faces[j];
                    foreach(int index in f.getPontos())
                    {
                        if(index == i)
                        {
                            vnFaces.Add(vn_faces[j]);
                            break;
                        }
                    }
                }
                double x = 0;
                double y = 0;
                double z = 0;
                foreach(Vector3 vnf in vnFaces)
                {
                    x += vnf.getX();
                    y += vnf.getY();
                    z += vnf.getZ();
                }
                x /= vnFaces.Count;
                y /= vnFaces.Count;
                z /= vnFaces.Count;

                vn_pts[i] = new Vector3(x, y, z);
                vn_pts[i].normalize();
            }
        }
        private void updateVNVertice_rotacao(List<Vector3>pts_normal ,double tx,double ty,double tz)
        {           
            double[,] mat;
            tx = tx * Math.PI / 180;
            ty = ty * Math.PI / 180;
            tz = tz * Math.PI / 180;

            double [,]matACTemp = new double[4, 4];
            matACTemp[0, 0] = 1;
            matACTemp[1, 1] = 1;
            matACTemp[2, 2] = 1;
            matACTemp[3, 3] = 1;

            for (int i = 0; i < pts_normal.Count;i++)
            {
                
                if (tx != 0)
                {
                    mat = new double[4, 4];
                    mat[0, 0] = 1;
                    mat[1, 1] = Math.Cos(tx);
                    mat[1, 2] = -Math.Sin(tx);
                    mat[2, 1] = Math.Sin(tx);
                    mat[2, 2] = Math.Cos(tx);
                    mat[3, 3] = 1;
                    matACTemp = Util.multiplicar(mat, matACTemp);

                }
                if (ty != 0)
                {
                    mat = new double[4, 4];
                    mat[0, 0] = Math.Cos(ty);
                    mat[0, 2] = Math.Sin(ty);
                    mat[2, 0] = -Math.Sin(ty);
                    mat[2, 2] = Math.Cos(ty);
                    mat[1, 1] = 1;
                    mat[3, 3] = 1;
                    matACTemp = Util.multiplicar(mat, matACTemp);
                }
                if (tz != 0)
                {
                    mat = new double[4, 4];
                    mat[0, 0] = Math.Cos(tz);
                    mat[0, 1] = -Math.Sin(tz);
                    mat[1, 0] = Math.Sin(tz);
                    mat[1, 1] = Math.Cos(tz);
                    mat[2, 2] = 1;
                    mat[3, 3] = 1;
                    matACTemp = Util.multiplicar(mat, matACTemp);
                }
                Vector3 p = pts_normal[i];
                double x = matACTemp[0, 0] * p.getX() + matACTemp[0, 1] * p.getY() + matACTemp[0, 2] * p.getZ() + matACTemp[0, 3];
                double y = matACTemp[1, 0] * p.getX() + matACTemp[1, 1] * p.getY() + matACTemp[1, 2] * p.getZ() + matACTemp[1, 3];
                double z = matACTemp[2, 0] * p.getX() + matACTemp[2, 1] * p.getY() + matACTemp[2, 2] * p.getZ() + matACTemp[2, 3];
                pts_normal[i] = new Vector3(x, y, z);
            }
        }
        #endregion
        #region Transformações
        public void translacao(double tx, double ty, double tz)
        {
            double[,] mat = new double[4, 4];
            mat[0, 0] = 1;
            mat[1, 1] = 1;
            mat[2, 2] = 1;
            mat[3, 3] = 1;
            mat[0, 3] = tx;
            mat[1, 3] = ty;
            mat[2, 3] = tz;
            matAc = Util.multiplicar(mat, matAc);
            novosPontos(matAc, pontosAtuais);
        }

        public List<Vector3> translacao(double tx, double ty, double tz, List<Vector3> pts)
        {
            List<Vector3> novos_pts = new List<Vector3>();
            double[,] mat = new double[4, 4];
            mat[0, 0] = 1;
            mat[1, 1] = 1;
            mat[2, 2] = 1;
            mat[3, 3] = 1;
            mat[0, 3] = tx;
            mat[1, 3] = ty;
            mat[2, 3] = tz;

            foreach (Vector3 p in pts)
            {
                double x = mat[0, 0] * p.getX() + mat[0, 1] * p.getY() + mat[0, 2] * p.getZ() + mat[0, 3];
                double y = mat[1, 0] * p.getX() + mat[1, 1] * p.getY() + mat[1, 2] * p.getZ() + mat[1, 3];
                double z = mat[2, 0] * p.getX() + mat[2, 1] * p.getY() + mat[2, 2] * p.getZ() + mat[2, 3];
                Vector3 np = new Vector3(x, y, z);
                novos_pts.Add(np);
            }
            return novos_pts;
        }

        public void escala(double tx, double ty, double tz)
        {
            double[,] mat = new double[4, 4];
            if (tx == 0)
                tx = 1;
            if (ty == 0)
                ty = 1;
            if (tz == 0)
                tz = 1;

            mat[0, 0] = tx;
            mat[1, 1] = ty;
            mat[2, 2] = tz;
            mat[3, 3] = 1;
            matAc = Util.multiplicar(mat, matAc);
            novosPontos(matAc, pontosAtuais);
        }
        public void rotacao(double tx, double ty, double tz)
        {
            double[,] mat;
            tx = tx * Math.PI / 180;
            ty = ty * Math.PI / 180;
            tz = tz * Math.PI / 180;
            if (tx != 0)
            {
                mat = new double[4, 4];
                mat[0, 0] = 1;
                mat[1, 1] = Math.Cos(tx);
                mat[1, 2] = -Math.Sin(tx);
                mat[2, 1] = Math.Sin(tx);
                mat[2, 2] = Math.Cos(tx);
                mat[3, 3] = 1;
                matAc = Util.multiplicar(mat, matAc);
                novosPontos(matAc, pontosAtuais);
            }
            if (ty != 0)
            {
                mat = new double[4, 4];
                mat[0, 0] = Math.Cos(ty);
                mat[0, 2] = Math.Sin(ty);
                mat[2, 0] = -Math.Sin(ty);
                mat[2, 2] = Math.Cos(ty);
                mat[1, 1] = 1;
                mat[3, 3] = 1;
                matAc = Util.multiplicar(mat, matAc);
                novosPontos(matAc, pontosAtuais);
            }
            if (tz != 0)
            {
                mat = new double[4, 4];
                mat[0, 0] = Math.Cos(tz);
                mat[0, 1] = -Math.Sin(tz);
                mat[1, 0] = Math.Sin(tz);
                mat[1, 1] = Math.Cos(tz);
                mat[2, 2] = 1;
                mat[3, 3] = 1;
                matAc = Util.multiplicar(mat, matAc);
                novosPontos(matAc, pontosAtuais);
            }
            updateVNVertice_rotacao(vn_pts,tx,ty,tz);
            updateVNFaces();
        }
        public void novosPontos(double[,] mat, List<Vector3> pts)
        {
            for (int i = 0; i < pontosOriginais.Count; i++)
            {
                Vector3 p = pontosOriginais[i];
                double x = mat[0, 0] * p.getX() + mat[0, 1] * p.getY() + mat[0, 2] * p.getZ() + mat[0, 3];
                double y = mat[1, 0] * p.getX() + mat[1, 1] * p.getY() + mat[1, 2] * p.getZ() + mat[1, 3];
                double z = mat[2, 0] * p.getX() + mat[2, 1] * p.getY() + mat[2, 2] * p.getZ() + mat[2, 3];
                pts[i].setX(x);
                pts[i].setY(y);
                pts[i].setZ(z);
            }
        }
        #endregion
        #region PontosProjeção
        public List<Vector3> novosPontosPorjecaoViewMiniatura(double escalaW, double escalaH)
        {
            List<Vector3> ptsProjecao = new List<Vector3>();

            double[,] mat = new double[4, 4];

            mat[0, 0] = escalaW;
            mat[1, 1] = escalaH;
            mat[2, 2] = escalaW;
            mat[3, 3] = 1;

            foreach (Vector3 p in pontosAtuais)
            {
                double x = mat[0, 0] * p.getX() + mat[0, 1] * p.getY() + mat[0, 2] * p.getZ() + mat[0, 3];
                double y = mat[1, 0] * p.getX() + mat[1, 1] * p.getY() + mat[1, 2] * p.getZ() + mat[1, 3];
                double z = mat[2, 0] * p.getX() + mat[2, 1] * p.getY() + mat[2, 2] * p.getZ() + mat[2, 3];
                Vector3 np = new Vector3(x, y, z);
                ptsProjecao.Add(np);
            }

            return ptsProjecao;
        }
        public List<Vector3> novosPontosProjecaoObliquas(float l, float a)
        {
            Vector3 c = getCentro();
            List<Vector3> ptsProjecao = translacao(-c.getX(), -c.getY(), -c.getZ(), pontosAtuais);

            double[,] mat = new double[4, 4];
            mat[0, 0] = 1;
            mat[0, 2] = Math.Cos(a) * l;
            mat[1, 1] = 1;
            mat[1, 2] = Math.Sin(a) * l;
            mat[3, 3] = 1;

            for (int i = 0; i < ptsProjecao.Count; i++)
            {
                Vector3 p = ptsProjecao[i];
                double x = mat[0, 0] * p.getX() + mat[0, 1] * p.getY() + mat[0, 2] * p.getZ() + mat[0, 3];
                double y = mat[1, 0] * p.getX() + mat[1, 1] * p.getY() + mat[1, 2] * p.getZ() + mat[1, 3];
                double z = mat[2, 0] * p.getX() + mat[2, 1] * p.getY() + mat[2, 2] * p.getZ() + mat[2, 3];
                ptsProjecao[i] = new Vector3(x, y, z);
            }

            ptsProjecao = translacao(c.getX(), c.getY(), c.getZ(), ptsProjecao);
            return ptsProjecao;
        }
        public List<Vector3> novosPontosPerspectiva(float d)
        {
            Vector3 c = getCentro();
            List<Vector3> ptsProjecao = new List<Vector3>();
            //d *= -1;
            double[,] mat = new double[4, 4];
            mat[0, 0] = d;
            mat[1, 1] = d;
            mat[3, 2] = 1;
            mat[3, 3] = d;

            for (int i = 0; i < pontosAtuais.Count; i++)
            {
                Vector3 p = pontosAtuais[i];
                /*
                double x = mat[0, 0] * p.getX() + mat[0, 1] * p.getY() + mat[0, 2] * p.getZ() + mat[0, 3] * p.getZ();
                double y = mat[1, 0] * p.getX() + mat[1, 1] * p.getY() + mat[1, 2] * p.getZ() + mat[1, 3] * p.getZ();
                double z = mat[2, 0] * p.getX() + mat[2, 1] * p.getY() + mat[2, 2] * p.getZ() + mat[2, 3] * p.getZ();
                          */      

                double x = (p.getX() * d) / (p.getZ() + d);
                double y = (p.getY() * d) / (p.getZ() + d);
                double z = 0;

                ptsProjecao.Add(new Vector3(x, y, z));
            }
            return ptsProjecao;
        }
        #endregion
        #region Desenha Wire
        private unsafe void drawWireXY(Bitmap img, Color c, BitmapData bmpData, int limW, int limH, List<Vector3> ptsDraw, bool ocultar)
        {
            for (int pf = 0; pf < faces.Count; pf++)
            {
                if (ocultar && vn_faces[pf].getZ() < 0)
                    continue;
                Face f = faces[pf];
                List<int> pts = f.getPontos();
                Vector3 p1;
                Vector3 p2;
                for (int i = 0; i < pts.Count - 1; i++)
                {

                    p1 = ptsDraw[pts[i]];
                    p2 = ptsDraw[pts[i + 1]];
                    Util.reta2D((int)p1.getX(), (int)p1.getY(), (int)p2.getX(), (int)p2.getY(), img, c, bmpData, limW, limH);
                }
                p1 = ptsDraw[pts[0]];
                p2 = ptsDraw[pts[pts.Count - 1]];
                Util.reta2D((int)p1.getX(), (int)p1.getY(), (int)p2.getX(), (int)p2.getY(), img, c, bmpData, limW, limH);
            }
        }
        private unsafe void drawWireXZ(Bitmap img, Color c, BitmapData bmpData, int limW, int limH, List<Vector3> ptsDraw, bool ocultar)
        {
            for (int pf = 0; pf < faces.Count; pf++)
            {
                if (ocultar && vn_faces[pf].getZ() < 0)
                    continue;
                Face f = faces[pf];
                List<int> pts = f.getPontos();
                Vector3 p1;
                Vector3 p2;
                for (int i = 0; i < pts.Count - 1; i++)
                {
                    p1 = ptsDraw[pts[i]];
                    p2 = ptsDraw[pts[i + 1]];
                    Util.reta2D((int)p1.getX(), (int)p1.getZ(), (int)p2.getX(), (int)p2.getZ(), img, c, bmpData, limW, limH);
                }
                p1 = ptsDraw[pts[0]];
                p2 = ptsDraw[pts[pts.Count - 1]];
                Util.reta2D((int)p1.getX(), (int)p1.getZ(), (int)p2.getX(), (int)p2.getZ(), img, c, bmpData, limW, limH);
            }
        }
        private unsafe void drawWireYZ(Bitmap img, Color c, BitmapData bmpData, int limW, int limH, List<Vector3> ptsDraw, bool ocultar)
        {
            for (int pf = 0; pf < faces.Count; pf++)
            {
                if (ocultar && vn_faces[pf].getZ() < 0)
                    continue;
                Face f = faces[pf];
                List<int> pts = f.getPontos();
                Vector3 p1;
                Vector3 p2;
                for (int i = 0; i < pts.Count - 1; i++)
                {
                    p1 = ptsDraw[pts[i]];
                    p2 = ptsDraw[pts[i + 1]];
                    Util.reta2D((int)p1.getY(), (int)p1.getZ(), (int)p2.getY(), (int)p2.getZ(), img, c, bmpData, limW, limH);
                }
                p1 = ptsDraw[pts[0]];
                p2 = ptsDraw[pts[pts.Count - 1]];
                Util.reta2D((int)p1.getY(), (int)p1.getZ(), (int)p2.getY(), (int)p2.getZ(), img, c, bmpData, limW, limH);
            }
        }
        #endregion
        #region Projeções
        public void drawOrtografica(Bitmap img,Color c, Color a, Color d, Color e, int op,int opDesenho, bool ocultar,Vector3 luz, double intensidade, byte n)
        {
            updateVNFaces();
            long ini = Environment.TickCount;
            int H = img.Height;
            int W = img.Width;
            BitmapData bmpData = img.LockBits(new Rectangle(0, 0, W, H), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);
            switch (op)
            {                
                case 2:
                    drawWireXY(img, c, bmpData, W, H, novosPontosPorjecaoViewMiniatura(escalaW, escalaH), ocultar);
                    break;
                case 3:
                    drawWireXZ(img, c, bmpData, W, H, novosPontosPorjecaoViewMiniatura(escalaW, escalaH), ocultar);
                    break;
                case 4:
                    drawWireYZ(img, c, bmpData, W, H, novosPontosPorjecaoViewMiniatura(escalaW, escalaH), ocultar);
                    break;
            }
            
            if (op == 1)
                switch (opDesenho)
                {
                    case 1:
                        drawWireXY(img, c, bmpData, W, H, pontosAtuais, ocultar);
                        break;
                    case 2:         
                        flat(img,c, a,d,e, bmpData, W, H, pontosAtuais, ocultar,luz,intensidade,n);
                        break;
                    case 3:
                        
                        break;
                    case 4:
                        Gouraud(img, c, a, d, e, bmpData, W, H, pontosAtuais, ocultar, luz, intensidade, n);
                        break;
                }

            img.UnlockBits(bmpData);
            Console.WriteLine("Ortogarfica:"+ (Environment.TickCount - ini));
        }
        public unsafe void drawWireOblique(Bitmap img, Color c, Color a, Color d, Color e, float l,int opDesenho, bool ocultar,Vector3 luz, double intensidade, byte n)
        {
            long ini = Environment.TickCount;
            int H = img.Height;
            int W = img.Width;
            BitmapData bmpData = img.LockBits(new Rectangle(0, 0, W, H), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);

            List<Vector3> ptsProjecao = novosPontosProjecaoObliquas(l, 45);
            updateVNFaces(ptsProjecao);
            updateVNVertice_rotacao(vn_pts, 45, 0, 0);
            switch (opDesenho)
            {
                case 1:
                    drawWireXY(img, c, bmpData, W, H, ptsProjecao, ocultar);
                    break;
                case 2:
                    flat(img,c,a, d, e, bmpData, W, H, ptsProjecao, ocultar,luz, intensidade,n);
                    break;
                case 3:
                    
                    break;
                case 4:
                    Gouraud(img, c, a, d, e, bmpData, W, H, ptsProjecao, ocultar, luz, intensidade, n);
                    break;
            }

            img.UnlockBits(bmpData);

            Console.WriteLine("Oblique:" + (Environment.TickCount - ini) + " l:" + l);
        }
        public unsafe void drawWirePerspectiva(Bitmap img, Color c, Color a, Color df, Color e, float d,int opDesenho, bool ocultar, Vector3 luz, double intensidade, byte n)
        {
            long ini = Environment.TickCount;
            int H = img.Height;
            int W = img.Width;
            BitmapData bmpData = img.LockBits(new Rectangle(0, 0, W, H), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);

            List<Vector3> ptsProjecao = novosPontosPerspectiva(d);
            updateVNFaces(ptsProjecao);
            switch (opDesenho)
            {
                case 1:
                    drawWireXY(img, c, bmpData, W, H, ptsProjecao, ocultar);
                    break;
                case 2:
                    flat(img,c, a, df, e, bmpData, W, H, ptsProjecao, ocultar, luz, intensidade, n);
                    break;
                case 3:

                    break;
                case 4:
                    Gouraud(img, c, a, df, e, bmpData, W, H, ptsProjecao, ocultar, luz, intensidade, n);
                    break;
            }

            img.UnlockBits(bmpData);
            Console.WriteLine("Perspectiva:" + (Environment.TickCount - ini));
        }
        #endregion
        #region Iluminação        
        private static void heap(List<Aresta> AET)
        {
            int TL2 = AET.Count;
            int e, d, p;
            int pos;
            Aresta aux;
            while (TL2 > 1)
            {
                for (p = TL2 / 2 - 1; p >= 0; p--)
                {
                    e = p * 2 + 1;
                    d = e + 1;
                    pos = e;
                    if (d < TL2 && AET[d].getXmin() > AET[e].getXmin())
                        pos = d;
                    if (AET[p].getXmin() < AET[pos].getXmin())
                    {
                        aux = AET[p];
                        AET[p] = AET[pos];
                        AET[pos] = aux;
                    }
                }
                TL2--;
                aux = AET[0];
                AET[0] = AET[TL2];
                AET[TL2] = aux;
            }
        }        
        private List<List<Aresta>> geraET(List<Vector3> pts, Face f, int limH, bool ocultar)
        {

            List<List<Aresta>> ET = new List<List<Aresta>>();
            for (int i = 0; i < limH; i++)
                ET.Add(new List<Aresta>());
            Aresta aresta;

            List<int> indexPts = f.getPontos();
            for (int i = 0; i < indexPts.Count - 1; i++)
            {
                aresta = new Aresta(pts[indexPts[i]], pts[indexPts[(i + 1)]]);
                if (aresta.getYmin() < limH)
                    ET[aresta.getYmin()].Add(aresta);
            }
            aresta = new Aresta(pts[indexPts[indexPts.Count - 1]], pts[indexPts[0]]);
            if (aresta.getYmin() < limH)
                ET[aresta.getYmin()].Add(aresta);

            return ET;
        }
        private unsafe void flat(Bitmap img,Color c, Color am, Color d, Color e, BitmapData bmpData, int limW, int limH, List<Vector3> pts, bool ocultar,Vector3 l, double intensidade, byte n)
        {
            l.normalize();
            long ini = Environment.TickCount;
            Vector3 obs = new Vector3(0, 0, 1);          
            
            int W = img.Width;
            int padding = bmpData.Stride - (W * 3);
            byte* ptrIni = (byte*)bmpData.Scan0.ToPointer();
            
            for (int pf = 0; pf < faces.Count; pf++)
            {
                Face f = faces[pf];

               if (ocultar && vn_faces[pf].getZ() < 0)
                    continue;

                Vector3 PFace = pts[f.getPontos()[0]];

                //Vector3 l = new Vector3(luz.getX() - PFace.getX(), luz.getY() - PFace.getY() , luz.getZ() - PFace.getZ());
               // l.normalize();
               
                Vector3 h = new Vector3(l.getX() + obs.getX(), l.getY() + obs.getY(), l.getZ() + obs.getZ());
                h.normalize();    
                
                double LN = l.getX() * vn_faces[pf].getX() + l.getY() * vn_faces[pf].getY() + l.getZ() * vn_faces[pf].getZ();
                LN = (l.norma() * vn_faces[pf].norma()) == 0 ? 0 : LN / (l.norma() * vn_faces[pf].norma());
                double HN = Math.Pow(h.getX() * vn_faces[pf].getX() + h.getY() * vn_faces[pf].getY() + h.getZ() * vn_faces[pf].getZ() , n);     
                double ar = am.R * 0.1;
                double ag = am.G * 0.1;
                double ab = am.B * 0.1;                                
                double dr = LN * d.R * intensidade;
                double dg = LN * d.G * intensidade;
                double db = LN * d.B * intensidade;
                double er = HN * e.R * intensidade;
                double eg = HN * e.G * intensidade;
                double eb = HN * e.B * intensidade;               
                double cr = ar + dr + er;
                double cg = ag + dg + eg;
                double cb = ab + db + eb;
                cr = cr > 255 ? 255 : cr;
                cg = cg > 255 ? 255 : cg;
                cb = cb > 255 ? 255 : cb;                
                cr = cr < 1 ? 0 : cr;
                cg = cg < 1 ? 0 : cg;
                cb = cb < 1 ? 0 : cb;
                
                Color c_luz = Color.FromArgb((int)cr, (int)cg, (int)cb);
                
                //Color c_luz = geraCor(vn_faces[pf], l, h, am, d, e, intensidade, n);

                List<List<Aresta>> ET = geraET(pts,f, limH, ocultar);
                List<Aresta> AET = new List<Aresta>();                                           

                int y = 0, x;
                while (y < limH && ET[y].Count == 0)
                    y++;

                if (y < limH)
                    foreach (Aresta a in ET[y])
                        AET.Add(a);

                while (AET.Count > 0)
                {
                    for (int i = 0; i < AET.Count; i++)
                        if ((int)AET[i].getYmax() == y)
                            AET.Remove(AET[i--]);
                    heap(AET);
                    for (int i = 0; i < AET.Count - 1; i += 2)
                    {
                        Aresta a = AET[i];
                        Aresta b = AET[i + 1];
                        for (x = (int)Math.Floor(a.getXmin()); x < Math.Ceiling(b.getXmin()); x++)
                        {
                            Util.setPixel(ptrIni, x, y, W, padding, c_luz, limW, limH);
                        }   
                        a.setXmin(a.getXmin() + a.getIncX());
                        b.setXmin(b.getXmin() + b.getIncX());
                    }
                    y++;
                    if (y < limH)
                        foreach (Aresta a in ET[y])
                            AET.Add(a);
                }
            }
            Console.WriteLine("Flat:" + (Environment.TickCount - ini));
        }     
        public Color geraCor(Vector3 vn,Vector3 l,Vector3 h,Color a,Color d,Color e, double intensidade,byte n)
        {            
            double LN = l.getX() * vn.getX() + l.getY() * vn.getY() + l.getZ() * vn.getZ();
            LN = (l.norma() * vn.norma()) == 0 ? 0 : LN / (l.norma() * vn.norma());
            double HN = Math.Pow(h.getX() * vn.getX() + h.getY() * vn.getY() + h.getZ() * vn.getZ(), n);
            double ar = a.R * 0.1;
            double ag = a.G * 0.1;
            double ab = a.B * 0.1;
            double dr = LN * d.R * intensidade;
            double dg = LN * d.G * intensidade;
            double db = LN * d.B * intensidade;
            double er = HN * e.R * intensidade;
            double eg = HN * e.G * intensidade;
            double eb = HN * e.B * intensidade;
            double cr = ar + dr + er;
            double cg = ag + dg + eg;
            double cb = ab + db + eb;
            cr = cr > 255 ? 255 : cr;
            cg = cg > 255 ? 255 : cg;
            cb = cb > 255 ? 255 : cb;
            cr = cr < 1 ? 0 : cr;
            cg = cg < 1 ? 0 : cg;
            cb = cb < 1 ? 0 : cb;

            return Color.FromArgb((int)cr, (int)cg, (int)cb);
        }
        private unsafe void Gouraud(Bitmap img, Color c, Color am, Color d, Color e, BitmapData bmpData, int limW, int limH, List<Vector3> pts, bool ocultar, Vector3 l, double intensidade, byte n)
        {
            Vector3 obs = new Vector3(0, 0, 1);
            l.normalize();
            Vector3 h = new Vector3(l.getX() + obs.getX(), l.getY() + obs.getY(), l.getZ() + obs.getZ());
            h.normalize();

            int W = img.Width;
            int padding = bmpData.Stride - (W * 3);
            byte* ptrIni = (byte*)bmpData.Scan0.ToPointer();
                        
            for (int pf = 0; pf < faces.Count; pf++)
            {                
                Face f = faces[pf];
                if (ocultar && vn_faces[pf].getZ() < 0)
                    continue;
                List<List<Aresta>> ET = new List<List<Aresta>>();
                for (int i = 0; i < limH; i++)
                    ET.Add(new List<Aresta>());
                Aresta aresta;
                Color corI,corF;
                List<int> indexPts = f.getPontos();
                for (int i = 0; i < indexPts.Count - 1; i++)
                {
                    corI = geraCor(vn_pts[indexPts[i]],l,h,am,d,e,intensidade,n);
                    corF = geraCor(vn_pts[indexPts[i + 1]], l, h, am, d, e, intensidade, n);
                    aresta = new Aresta(pts[indexPts[i]], pts[indexPts[(i + 1)]],corI,corF);
                    if (aresta.getYmin() < limH)
                        ET[aresta.getYmin()].Add(aresta);
                }
                corI = geraCor(vn_pts[indexPts[indexPts.Count - 1]], l, h, am, d, e, intensidade, n);
                corF = geraCor(vn_pts[indexPts[0]], l, h, am, d, e, intensidade, n);
                aresta = new Aresta(pts[indexPts[indexPts.Count - 1]], pts[indexPts[0]], corI, corF);
                if (aresta.getYmin() < limH)
                    ET[aresta.getYmin()].Add(aresta);                      

            List<Aresta> AET = new List<Aresta>();
            int y = 0, x;
            while (y < limH && ET[y].Count == 0)
                y++;

            if (y < limH)
                foreach (Aresta a in ET[y])
                    AET.Add(a);

                while (AET.Count > 0)
                {
                    for (int i = 0; i < AET.Count; i++)
                        if ((int)AET[i].getYmax() == y)
                            AET.Remove(AET[i--]);
                    heap(AET);
                    for (int i = 0; i < AET.Count - 1; i += 2)
                    {
                        Aresta a = AET[i];
                        Aresta b = AET[i + 1];
                        double deltaX = (a.getXmin() - b.getXmin());
                        deltaX = deltaX < 0 ? deltaX * -1 : deltaX;
                        int cr = deltaX > 0 ? (int)((b.getCor().R - a.getCor().R) / deltaX) : 0;
                        int cg = deltaX > 0 ? (int)((b.getCor().G - a.getCor().G) / deltaX) : 0;
                        int cb = deltaX > 0 ? (int)((b.getCor().B - a.getCor().B) / deltaX) : 0;
                        cr = cr < 0 ? 0 : cr;
                        cg = cg < 0 ? 0 : cg;
                        cb = cb < 0 ? 0 : cb;
                        cr = cr > 255 ? 255 : cr;
                        cg = cg > 255 ? 255 : cg;
                        cb = cb > 255 ? 255 : cb;

                        for (x = (int)Math.Floor(a.getXmin()); x < Math.Ceiling(b.getXmin()); x++)
                        {                                    
                            Util.setPixel(ptrIni, x, y, W, padding, a.getCor(), limW, limH);
                            a.incCor(cr, cg, cb);
                        }
                        a.setXmin(a.getXmin() + a.getIncX());                       
                        b.setXmin(b.getXmin() + b.getIncX());
                        a.incCor();
                        b.incCor();
                    }
                    y++;
                    if (y < limH)
                        foreach (Aresta a in ET[y])
                            AET.Add(a);
                }
            }            
        }
        #endregion
    }
}