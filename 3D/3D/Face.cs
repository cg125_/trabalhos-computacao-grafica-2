﻿using System.Collections.Generic;

namespace _3D
{
    class Face
    {
        private List<int> pontos;

        public Face()
        {
            pontos = new List<int>();
        }

        public Face(List<int> pts)
        {
            pontos = pts;
        }
        public List<int> getPontos() { return pontos; }
    }
}
